########################### Vacuum Angle Valve Bi Stable Control ############################


#-############################
#- COMMAND BLOCK
#-############################
define_command_block()

add_digital("AutoCmd",                         PV_NAME="AutoModeCmd",                   PV_DESC="Set control mode to AUTOMATIC")
add_digital("ManuCmd",                         PV_NAME="ManualModeCmd",                 PV_DESC="Set control mode to MANUAL")
add_digital("CloseManuCmd",                    PV_NAME="ManualCloseCmd",                PV_DESC="Manual CLOSE command")
add_digital("OpenManuCmd",                     PV_NAME="ManualOpenCmd",                 PV_DESC="Manual OPEN command")
add_digital("BypassItlCmd",                    PV_NAME="OverrideITLckCmd",              PV_DESC="Override tripped interlock")
add_digital("BlockClosedCmd",                  PV_NAME="AutoOpenDisCmd",                PV_DESC="Disable automatic OPEN")
add_digital("ResetCmd",                        PV_NAME="RstErrorsCmd",                  PV_DESC="Reset errors and warnings")
#-----------------------------
add_digital("EPICSItlReq",                     PV_NAME="ITLck_EPICS_TrigCmd",           PV_DESC="Trigger EPICS interlock")
add_digital("ClearBypassCmd",                  PV_NAME="ClrOverrideITLckCmd",           PV_DESC="Clear interlock override")
add_digital("UnlockCmd",                       PV_NAME="AutoOpenEnaCmd",                PV_DESC="Enable automatic OPEN")
add_digital("ClearCountersCmd",                PV_NAME="ClrCountersCmd",                PV_DESC="Clear counters")


#-############################
#- STATUS BLOCK
#-############################
define_status_block()

add_major_alarm("ErrorSt",	"Tripped",         PV_NAME="ErrorR",                        PV_DESC="Error detected by the control function",  PV_ONAM="Error", ARCHIVE=True)
#-----------------------------
add_digital("ManuSt",                          PV_NAME="ManualModeR",                   PV_DESC="Control mode status",                     PV_ONAM="Manual",               PV_ZNAM="Auto")
add_digital("CloseSt",                         PV_NAME="ClosedR",                       PV_DESC="The valve is CLOSED",                     PV_ONAM="Closed", ARCHIVE=True)
add_digital("UndefinedSt",                     PV_NAME="UndefinedR",                    PV_DESC="The valve is neither OPEN nor CLOSED",    PV_ONAM="Undefined")
add_digital("OpenSt",                          PV_NAME="OpenR",                         PV_DESC="The valve is OPEN",                       PV_ONAM="Open", ARCHIVE=True)
add_digital("InterlockTriggerSt",			   PV_NAME="ITLckTrigR",                    PV_DESC="Interlock triggering status",             PV_ONAM="NominalState")
#-----------------------------
add_digital("OpenDQSt",                        PV_NAME="OpenDQ-RB",                     PV_DESC="Status of the OPEN digital output",       PV_ONAM="True",                 PV_ZNAM="False")
add_digital("CloseDQSt",                       PV_NAME="CloseDQ-RB",                    PV_DESC="Status of the CLOSE digital output",      PV_ONAM="True",                 PV_ZNAM="False")
add_digital("OpenManuSt",                      PV_NAME="OnManualOpenR",                 PV_DESC="Current OPEN command was MANUAL",         PV_ONAM="Manual Open")
add_digital("CloseManuSt",                     PV_NAME="OnManualCloseR",                PV_DESC="Current CLOSE command was MANUAL",        PV_ONAM="Manual Close")
add_digital("OpenAutoSt",                      PV_NAME="OnAutoOpenR",                   PV_DESC="Current OPEN command was AUTOMATIC",      PV_ONAM="Automatic Open")
add_digital("CloseAutoSt",                     PV_NAME="OnAutoCloseR",                  PV_DESC="Current CLOSE command was AUTOMATIC",     PV_ONAM="Automatic Close")
add_digital("LockedSt",                        PV_NAME="AutoOpenDisStR",                PV_DESC="Automatic OPEN toggle status",            PV_ONAM="Auto open Disabled",   PV_ZNAM="Auto open Enabled")
add_digital("WarningSt",                       PV_NAME="WarningR",                      PV_DESC="A warning is active",                     PV_ONAM="Warning")
add_digital("InvalidCommandSt",                PV_NAME="InvalidCommandR",               PV_DESC="Last command sent cannot be processed",   PV_ONAM="Invalid command")
add_digital("ValidSt",                         PV_NAME="ValidR",                        PV_DESC="Communication is valid",                  PV_ONAM="Valid",                PV_ZNAM="Invalid")
add_analog("InterlockSt",              "INT",  PV_NAME="ITLckStatR",                PV_DESC="Overal Interlock status", ARCHIVE=True)

add_digital("OpenItlHealthySt",                PV_NAME="ITLck_Open_HltyR",              PV_DESC="Open interlock is HEALTHY",               PV_ONAM="HEALTHY")
add_digital("OpenInterlockSt",                 PV_NAME="ITLck_Open_TrpR",   		PV_DESC="Open interlock is TRIPPED")
add_digital("OpenItlBypassSt",                 PV_NAME="ITLck_Open_OvRidnR",            PV_DESC="Open interlock is OVERRIDEN",             PV_ONAM="OVERRIDEN")
add_digital("DisableOpenInterlock",            PV_NAME="ITLck_Open_DisR",               PV_DESC="Open interlock is NOT CONFIGURED",        PV_ONAM="NOT CONFIGURED",      PV_ZNAM="CONFIGURED")

#-----------------------------

add_digital("CloseWItlHealhtySt",              PV_NAME="ITLck_Close_HltyR",             PV_DESC="Close interlock is HEALTHY",              PV_ONAM="HEALTHY")
add_digital("CloseInterlockSt",                PV_NAME="ITLck_Close_TrpR",   		PV_DESC="Close interlock is TRIPPED")
add_digital("CloseItlBypassSt",                PV_NAME="ITLck_Close_OvRidnR",           PV_DESC="Close interlock is OVERRIDEN",            PV_ONAM="OVERRIDEN")
add_digital("DisableCloseInterlock",           PV_NAME="ITLck_Close_DisR",              PV_DESC="Close interlock is NOT CONFIGURED",       PV_ONAM="NOT CONFIGURED",      PV_ZNAM="CONFIGURED")

add_analog("ValveSt",              "INT",      PV_NAME="StatR",                         PV_DESC="Valve status", ARCHIVE=True)

add_analog("WarningCode",          "BYTE",     PV_NAME="WarningCodeR",                  PV_DESC="Active warning code")

add_string("WarningMessage",       39,         PV_NAME="WarningMessageR",        	PV_DESC="Warning Message")

add_analog("ErrorCode",            "BYTE",     PV_NAME="ErrorCodeR",                    PV_DESC="Active error code", ARCHIVE=True)

add_string("ErrorMessage",         39,         PV_NAME="ErrorMessageR",        	        PV_DESC="Warning Message")

add_analog("RunTime",              "REAL",     PV_NAME="RunTimeR",                      PV_DESC="Runtime in hours", PV_EGU="h", ARCHIVE=True)

add_analog("OpenCounter",          "UDINT",    PV_NAME="OpenCounterR",                  PV_DESC="Number of opens", ARCHIVE=True)

