############################## Vacuum Pumping Group ##############################
##############################   Warm LINAC - MEBT  ##############################

define_template("MODE",  PV_ZRVL="0",  PV_ZRST="N/A",  PV_ONVL="1",  PV_ONST="Normal Pumping",  PV_TWVL="10",  PV_TWST="Manual Mode")


#-############################
#- COMMAND BLOCK
#-############################

define_command_block()

add_digital("AutoCmd",                                    PV_NAME="AutoModeCmd",                   PV_DESC="Set control mode to AUTOMATIC")
add_digital("ManuCmd",                                    PV_NAME="ManualModeCmd",                 PV_DESC="Set control mode to MANUAL")
add_digital("StopManuCmd",                                PV_NAME="ManualStopCmd",                 PV_DESC="Manual STOP command")
add_digital("StartManuCmd",                               PV_NAME="ManualStartCmd",                PV_DESC="Manual START command")
add_digital("BypassItlCmd",                               PV_NAME="OverrideITLckCmd",              PV_DESC="Override tripped interlock")
add_digital("BlockOffCmd",                                PV_NAME="AutoStartDisCmd",               PV_DESC="Disable automatic START")
add_digital("ResetCmd",                                   PV_NAME="RstErrorsCmd",                  PV_DESC="Reset errors and warnings")
#-----------------------------
add_digital("EnableHighVacuumStart",                      PV_NAME="VacStartEnaCmd",                PV_DESC="Pressure_Sector <= Pressure_Low-Vacuum")
add_digital("EnableLowVacuumStart",                       PV_NAME="ATMStartEnaCmd",                PV_DESC="Pressure_Sector > Pressure_Low-Vacuum")
add_digital("ClearAutoRestartCmd",                        PV_NAME="ClrAutoRestartCmd",             PV_DESC="Disable auto RESTART after some errors")
add_digital("AutoRestartCmd",                             PV_NAME="AutoRestartCmd",                PV_DESC="Enable auto RESTART after some errors")
add_digital("ClearBypassCmd",                             PV_NAME="ClrOverrideITLckCmd",           PV_DESC="Clear interlock override")
add_digital("UnlockCmd",                                  PV_NAME="AutoStartEnaCmd",               PV_DESC="Enable automatic START")
add_digital("ClearCountersCmd",                           PV_NAME="ClrCountersCmd",                PV_DESC="Clear counters")

add_enum("ModeCmd",                "BYTE",                PV_NAME="ModeCmd",                       PV_DESC="Automatic mode selection",                TEMPLATE="MODE")


#-############################
#- STATUS BLOCK
#-############################

define_status_block()

add_digital("ManuSt",                                     PV_NAME="ManualModeR",                   PV_DESC="Control mode status",                     PV_ONAM="Manual",               PV_ZNAM="Auto")
add_digital("OffSt",                                      PV_NAME="StoppedR",                      PV_DESC="The pumping group is STOPPED",            PV_ONAM="Stopped")
add_digital("AcceleratingSt",                             PV_NAME="AcceleratingR",                 PV_DESC="The pumping group is ACCELERATING",       PV_ONAM="Accelerating")
add_digital("OnSt",                                       PV_NAME="AtNominalSpdR",                 PV_DESC="The pumping group is at NOMINAL SPEED",   PV_ONAM="At Nominal Speed", ARCHIVE=True)
add_digital("ErrorSt",                                    PV_NAME="ErrorR",                        PV_DESC="Error detected by the control function",  PV_ONAM="Error", ARCHIVE=True)
add_digital("EnableLowVacuumStartSt",                     PV_NAME="ATMStartEnaStR",                PV_DESC="ATM start enabled status",                PV_ONAM="Enabled",              PV_ZNAM="Disabled")
add_digital("AutoRestartSt",                              PV_NAME="AutoRestartR",                  PV_DESC="Automatic RESTART toggle status",         PV_ONAM="Auto Restart Enabled", PV_ZNAM="Auto Restart Disabled")
add_major_alarm("InterlockTriggerSt",  "Tripped",         PV_NAME="ITLckTrigR",                    PV_DESC="Interlock triggering status",             PV_ONAM="NominalState",         ALARM_IF=False)
#-----------------------------
add_digital("NominalDQCmd",                               PV_NAME="StartDQ-RB",                    PV_DESC="Status of the START digital output",      PV_ONAM="True",                 PV_ZNAM="False")
add_digital("StartManuSt",                                PV_NAME="OnManualStartR",                PV_DESC="Current START command was MANUAL",        PV_ONAM="Manual Start")
add_digital("StartAutoSt",                                PV_NAME="OnAutoStartR",                  PV_DESC="Current START command was AUTOMATIC",     PV_ONAM="Automatic Start")
add_digital("LockedSt",                                   PV_NAME="AutoStartDisStR",               PV_DESC="Automatic START toggle status",           PV_ONAM="Auto Start Disabled",  PV_ZNAM="Auto Start Enabled")
add_digital("WarningSt",                                  PV_NAME="WarningR",                      PV_DESC="A warning is active",                     PV_ONAM="Warning")
add_digital("InvalidCommandSt",                           PV_NAME="InvalidCommandR",               PV_DESC="Last command sent cannot be processed",   PV_ONAM="Invalid command")
add_digital("Low Vacuum Pumping System Configuration",    PV_NAME="LowVacuumConfigR",              PV_DESC="No turbo pumps configured",               PV_ONAM="No VPT configured")
add_digital("ValidSt",                                    PV_NAME="ValidR",                        PV_DESC="Communication is valid",                  PV_ONAM="Valid",                PV_ZNAM="Invalid")

add_digital("HardWItlHealthySt",                          PV_NAME="ITLck_HW_HltyR",                PV_DESC="Hardware interlock is HEALTHY",           PV_ONAM="HEALTHY")
add_major_alarm("HardwareInterlockSt",  "TRIPPED",        PV_NAME="ITLck_HW_TrpR",                 PV_DESC="Hardware interlock is TRIPPED")
add_digital("HardWItlBypassSt",                           PV_NAME="ITLck_HW_OvRidnR",              PV_DESC="Hardware interlock is OVERRIDEN",         PV_ONAM="OVERRIDEN")
add_digital("Disable Hardware Interlock",                 PV_NAME="ITLck_HW_DisR",                 PV_DESC="Hardware interlock is NOT CONFIGURED",    PV_ONAM="NOT CONFIGURED",       PV_ZNAM="CONFIGURED")
add_digital("SoftWItlHealthySt",                          PV_NAME="ITLck_SW_HltyR",                PV_DESC="Software interlock is HEALTHY",           PV_ONAM="HEALTHY")
add_major_alarm("SoftwareInterlockSt",  "TRIPPED",        PV_NAME="ITLck_SW_TrpR",                 PV_DESC="Software interlock is TRIPPED")
add_digital("SoftWItlBypassSt",                           PV_NAME="ITLck_SW_OvRidnR",              PV_DESC="Software interlock is OVERRIDEN",         PV_ONAM="OVERRIDEN")
add_digital("Disable Software Interlock",                 PV_NAME="ITLck_SW_DisR",                 PV_DESC="Software interlock is NOT CONFIGURED",    PV_ONAM="NOT CONFIGURED",       PV_ZNAM="CONFIGURED")
#-----------------------------

add_analog("WarningCode",             "INT",              PV_NAME="WarningCodeR",                  PV_DESC="Active warning code")

add_analog("ErrorCode",               "INT",              PV_NAME="ErrorCodeR",                    PV_DESC="Active error code", ARCHIVE=True)

add_enum("ModeSt",                    "BYTE",             PV_NAME="ModeR",                         PV_DESC="Automatic mode status",  TEMPLATE="MODE")

add_analog("ObjectSt",                "BYTE",             PV_NAME="ObjStatCodeR",                  PV_DESC="")

add_analog("RunTime",                 "REAL",             PV_NAME="RunTimeR",                      PV_DESC="Runtime in hours", PV_EGU="h", ARCHIVE=True)

add_analog("StartCounter",            "UDINT",            PV_NAME="StartCounterR",                 PV_DESC="Number of starts", ARCHIVE=True)

add_verbatim("""
record(calc, "[PLCF#INSTALLATION_SLOT]:#ITLck_HW_TrpR")
{
field(SCAN, "1 second")
	field(DESC, "True if TRIPPED and not OVERRIDEN")
	field(INPA, "[PLCF#INSTALLATION_SLOT]:ITLck_HW_TrpR CP")
	field(INPB, "[PLCF#INSTALLATION_SLOT]:ITLck_HW_OvRidnR CP")
	field(CALC, "A && !B")
}
record(calc, "[PLCF#INSTALLATION_SLOT]:#ITLck_SW_TrpR")
{
field(SCAN, "1 second")
	field(DESC, "True if TRIPPED and not OVERRIDEN")
	field(INPA, "[PLCF#INSTALLATION_SLOT]:ITLck_SW_TrpR CP")
	field(INPB, "[PLCF#INSTALLATION_SLOT]:ITLck_SW_OvRidnR CP")
	field(CALC, "A && !B")
}
record(calc, "[PLCF#INSTALLATION_SLOT]:#ITLck_HW_HltyR")
{
field(SCAN, "1 second")
	field(DESC, "True if HEALTHY or DISABLED")
	field(INPA, "[PLCF#INSTALLATION_SLOT]:ITLck_HW_HltyR CP")
	field(INPB, "[PLCF#INSTALLATION_SLOT]:ITLck_HW_DisR CP")
	field(CALC, "A || B")
}
record(calc, "[PLCF#INSTALLATION_SLOT]:#ITLck_SW_HltyR")
{
field(SCAN, "1 second")
	field(DESC, "True if HEALTHY or DISABLED")
	field(INPA, "[PLCF#INSTALLATION_SLOT]:ITLck_SW_HltyR CP")
	field(INPB, "[PLCF#INSTALLATION_SLOT]:ITLck_SW_DisR CP")
	field(CALC, "A || B")
}

record(calcout, "[PLCF#INSTALLATION_SLOT]:#ITLck_HltyR")
{
field(SCAN, "1 second")
	field(DESC, "Interlocks are HEALTHY")
	field(INPA, "[PLCF#INSTALLATION_SLOT]:ITLck_DisR CP")
	field(INPB, "[PLCF#INSTALLATION_SLOT]:#ITLck_HW_HltyR CP")
	field(INPC, "[PLCF#INSTALLATION_SLOT]:#ITLck_SW_HltyR CP")

	field(CALC, "!A && B && C")
	field(OUT,  "[PLCF#INSTALLATION_SLOT]:ITLck_HltyR PP")
}
record(bi, "[PLCF#INSTALLATION_SLOT]:ITLck_HltyR")
{
	field(DESC, "Interlocks are HEALTHY")
	field(ONAM, "HEALTHY")
	field(DISP, "1")
}

record(calcout, "[PLCF#INSTALLATION_SLOT]:#ITLck_TrpR")
{
field(SCAN, "1 second")
	field(DESC, "At least one interlock is TRIPPED")
	field(INPA, "[PLCF#INSTALLATION_SLOT]:#ITLck_HW_TrpR CP")
	field(INPB, "[PLCF#INSTALLATION_SLOT]:#ITLck_SW_TrpR CP")

	field(CALC, "A || B")
	field(OUT,  "[PLCF#INSTALLATION_SLOT]:ITLck_TrpR PP")
}
record(bi, "[PLCF#INSTALLATION_SLOT]:ITLck_TrpR")
{
	field(DESC, "At least one interlock is TRIPPED")
	field(OSV,  "MAJOR")
	field(ONAM, "TRIPPED")
	field(DISP, "1")
}

record(calcout, "[PLCF#INSTALLATION_SLOT]:#ITLck_OvRidnR")
{
field(SCAN, "1 second")
	field(DESC, "At least one interlock is OVERRIDEN")
	field(INPA, "[PLCF#INSTALLATION_SLOT]:ITLck_TrpR CP")
	field(INPB, "[PLCF#INSTALLATION_SLOT]:ITLck_HW_OvRidnR CP")
	field(INPC, "[PLCF#INSTALLATION_SLOT]:ITLck_SW_OvRidnR CP")

	field(CALC, "!A && (B || C)")
	field(OUT,  "[PLCF#INSTALLATION_SLOT]:ITLck_OvRidnR PP")
}
record(bi, "[PLCF#INSTALLATION_SLOT]:ITLck_OvRidnR")
{
	field(DESC, "At least one interlock is OVERRIDEN")
	field(ONAM, "OVERRIDEN")
	field(DISP, "1")
}

record(calcout, "[PLCF#INSTALLATION_SLOT]:#ITLck_DisR")
{
field(SCAN, "1 second")
	field(DESC, "Interlocks are NOT CONFIGURED")
	field(INPA, "[PLCF#INSTALLATION_SLOT]:ITLck_HW_DisR CP")
	field(INPB, "[PLCF#INSTALLATION_SLOT]:ITLck_SW_DisR CP")

	field(CALC, "A && B")
	field(OUT,  "[PLCF#INSTALLATION_SLOT]:ITLck_DisR PP")
}
record(bi, "[PLCF#INSTALLATION_SLOT]:ITLck_DisR")
{
	field(DESC, "Interlocks are NOT CONFIGURED")
	field(ONAM, "NOT CONFIGURED")
	field(ZNAM, "CONFIGURED")
	field(DISP, "1")
}


record(calcout, "[PLCF#INSTALLATION_SLOT]:#ITLckStatR")
{
field(SCAN, "1 second")
	field(DESC, "Calculate combined interlock status")
	field(INPA, "[PLCF#INSTALLATION_SLOT]:ITLck_TrpR CP")
	field(INPB, "[PLCF#INSTALLATION_SLOT]:ITLck_OvRidnR CP")
	field(INPC, "[PLCF#INSTALLATION_SLOT]:ITLck_HltyR CP")
	field(INPD, "[PLCF#INSTALLATION_SLOT]:ITLck_DisR CP")

# Check that only one is true
	field(CALC, "E:=(A | B*2 | C*4 | D*8);F:=!(E&(E-1));F")
	field(OCAL, "F?(C + A*2 + B*3 + D*4):0")
	field(DOPT, "Use OCAL")
	field(OUT,  "[PLCF#INSTALLATION_SLOT]:ITLckStatR PP")
}
record(mbbi, "[PLCF#INSTALLATION_SLOT]:ITLckStatR")
{
	field(DESC, "Combined Interlock status")
	field(ZRST, "INVALID")
	field(ONST, "HEALTHY")
	field(TWST, "TRIPPED")
	field(TWSV, "MAJOR")
	field(THST, "OVERRIDEN")
	field(FRST, "NOT CONFIGURED")
}
""")

add_verbatim("""
record(calcout, "[PLCF#INSTALLATION_SLOT]:#StatR")
{
field(SCAN, "1 second")
	field(DESC, "Calculate pumping group status")
	field(INPA, "[PLCF#INSTALLATION_SLOT]:AcceleratingR CP")
	field(INPB, "[PLCF#INSTALLATION_SLOT]:AtNominalSpdR CP")
	field(INPC, "[PLCF#INSTALLATION_SLOT]:StoppedR CP")
	field(INPD, "[PLCF#INSTALLATION_SLOT]:ErrorR CP")
	field(INPE, "[PLCF#INSTALLATION_SLOT]:ValidR CP MSS")

# Check that only one is true. Error is a bit special though
	field(CALC, "F:=(A | B*2 | C*4 | D*8);G:=!(F&(F-1));G")
	field(OCAL, "E?(G?(A + B*2 + C*3 + D*4):D*4):0")
	field(DOPT, "Use OCAL")
	field(OUT,  "[PLCF#INSTALLATION_SLOT]:StatR PP MSS")
}
record(mbbi, "[PLCF#INSTALLATION_SLOT]:StatR")
{
	field(DESC, "Pumping group status")
	field(ZRST, "INVALID")
	field(ONST, "ACCELERATING")
	field(TWST, "AT NOMINAL SPEED")
	field(THST, "STOPPED")
	field(FRST, "ERROR")
	field(FRSV, "MAJOR")
}
""")


#-############################
#- METADATA
#-############################
#- ERRORS
define_metadata("error", MD_SCRIPT='css_code2msg.py', CODE_TYPE='error')
#-
add_metadata(99, 'Hardware Interlock')
add_metadata(98, 'Software Interlock')
add_metadata(97, 'Turbo-Pumps Controller Error')
add_metadata(96, 'Power Supply Error')
add_metadata(95, 'High Vacuum Manifold Vented - Pressure Interlock')
add_metadata(94, 'Low Vacuum Manifold Vented - Pressure Interlock')
add_metadata(92, 'Primary Pump Error')
add_metadata(91, 'Primary Pump / Valve Error')
add_metadata(90, 'Turbo-Pumps Not Available')
add_metadata(89, 'Max Auto-Restart')
add_metadata(88, 'Back-Up Primary Pumping System Error')
add_metadata(15, 'Mode Selection Error - Vacuum Sector Vented')
add_metadata(14, 'Mode Selection Error - Vacuum Sector Under Vacuum')
add_metadata(13, 'Primary Pumping System: Rescue Primary System is Off')
add_metadata(12, 'Primary Pumping System: Rescue Primary Not Available')
add_metadata(11, 'Primary Pumping System: Primary Pump / Valve Error')
add_metadata(10, 'Primary Pumping System: Start Disable')
add_metadata( 9, 'VPSU-0020 400VAC 3-Phases Power Supply Tripped')
#-
#- WARNINGS
define_metadata("warning", MD_SCRIPT='css_code2msg.py', CODE_TYPE='warning')
#-
add_metadata(99, 'Service Mode (Leak Detection) Over Time')
add_metadata(19, 'High Vacuum Pumping Starting Not Possible - All Systems are Locked')
add_metadata(18, '1st High Vacuum Pumping System Error - Angle Valve Position Error')
add_metadata(17, '2nd High Vacuum Pumping System Error - Angle Valve Position Error')
add_metadata(16, '3rd High Vacuum Pumping System Error - Angle Valve Position Error')
add_metadata(15, '4th High Vacuum Pumping System Error - Angle Valve Position Error')
add_metadata(14, '5th High Vacuum Pumping System Error - Angle Valve Position Error')
add_metadata(11, 'Atmospheric & Vacuum Starting are Enabled')
add_metadata(10, 'No Starting Enabled')
add_metadata( 9, 'Atmospheric Starting is Enabled')
add_metadata( 8, 'VPSU-0020 24VDC Power Supply Overload')
add_metadata( 7, 'VPSU-0020 24VDC Power Supply Tripped')
add_metadata( 6, 'VPSU-0010 24VDC Power Supply Overload')
add_metadata( 5, 'VPSU-0010 24VDC Power Supply Tripped')
add_metadata( 4, 'VPSU-0010 400VAC 3-Phases Power Supply Tripped')
add_metadata( 3, 'Automatic Restart On-going')
add_metadata( 2, 'All Pumping Systems Locked - Starting Not Possible')
add_metadata( 1, 'No Mode Selected')
