# linac-vac-plc-def

This repository contains the definition files for the PLCs controlling the vacuum systems of the LINAC

The various definition files are:
*   [PLC-itlck.def](PLC-itlck.def)
*   [PLC-proc.def](PLC-proc.def)
*   [VACUUM_VAC-PLCIO.def](VACUUM_VAC-PLCIO.def)
*   [VACUUM_VAC-VPG.def](VACUUM_VAC-VPG.def)
*   [VACUUM_VAC-VPG-MEBT.def](VACUUM_VAC-VPG-MEBT.def)
*   [VACUUM_VAC-VPI.def](VACUUM_VAC-VPI.def)
*   [VACUUM_VAC-VPP_VAC-VPDP.def](VACUUM_VAC-VPP_VAC-VPDP.def)
*   [VACUUM_VAC-VPSU.def](VACUUM_VAC-VPSU.def)
*   [VACUUM_VAC-VPT.def](VACUUM_VAC-VPT.def)
*   [VACUUM_VAC-VVA_BYPASS_.def](VACUUM_VAC-VVA_BYPASS_.def)
*   [VACUUM_VAC-VVA_ISRC.def](VACUUM_VAC-VVA_ISRC.def)
*   [VACUUM_VAC-VVA_VAC-VVG.def](VACUUM_VAC-VVA_VAC-VVG.def)
*   [VACUUM_VAC-VVF.def](VACUUM_VAC-VVF.def)
*   [VACUUM_VAC-VVS.def](VACUUM_VAC-VVS.def)

## PLC-itlck.def

Contains logic to aggregate the health and validity information about the PLCIO modules that are connected to the Interlock PLC and present them in a single PV:

*   `PLCIOValidR` from the `ValidR` PVs of the PLCIOs
*   `PLCIOHltyR`  from the `HltyR` PVs of the PLCIOs
*   `PLCIOWarnR`  from the `StatusCodeR` PVs of the PLCIOs
*   `ValidR` from the `PLCIOValidR` and `PLCIOHltyR` PVs

**Needs to be updated when a new PLCIO is added**

## PLC-proc.def

Contains logic to aggregate the health and validity information about the PLCIO modules that are connected to the Process PLC and present them in a single PV:

*   `PLCIOValidR` from the `ValidR` PVs of the PLCIOs
*   `PLCIOHltyR`  from the `HltyR` PVs of the PLCIOs
*   `PLCIOWarnR`  from the `StatusCodeR` PVs of the PLCIOs
*   `ValidR` from the `PLCIOValidR` and `PLCIOHltyR` PVs

**Needs to be updated when a new PLCIO is added**

## VACUUM_VAC-PLCIO.def

Contains PVs for variables coming from the PLC for the PLCIO modules

## VACUUM_VAC-VPG.def

Contains PVs for variables coming from the PLC for the pumping groups

## VACUUM_VAC-VPG-MEBT.def

Contains PVs for variables coming from the PLC specifically for the MEBT pumping group

## VACUUM_VAC-VPI.def

Contains PVs for variables coming from the PLC for the ion pumps

## VACUUM_VAC-VPP_VAC-VPDP.def

Contains PVs for variables coming from the PLC for the primary pumps

## VACUUM_VAC-VPSU.def

Contains PVs for variables coming from the PLC for the power supplies

## VACUUM_VAC-VPT.def

Contains PVs for variables coming from the PLC for the turbo pumps

## VACUUM_VAC-VVA_BYPASS_.def

Contains PVs for variables coming from the PLC for the bypass angle valves

The following properties are used:
*   `VVAG_ITLck_HW_1` defines the device name that is used as the HW:1 trigger source
*   `VVAG_ITLck_HW_2` defines the device name that is used as the HW:2 trigger source
*   `VVAG_ITLck_HW_3` defines the device name that is used as the HW:3 trigger source
*   `VVAG_ITLck_Prs_1` defines the device name that is used as the Pressure:1 trigger source
*   `VVAG_ITLck_Prs_2` defines the device name that is used as the Pressure:2 trigger source
*   `VVAG_ITLck_Open` defines the device name that is used as the Open trigger source

If a trigger is not used then set the property to `-` (or the empty string once CCDB supports that)

## VACUUM_VAC-VVA_ISRC.def

Contains logic to map PVs from the Ion Source LPS system for the Ion source valve

## VACUUM_VAC-VVA_VAC-VVG.def

Contains PVs for variables coming from the PLC for the angle and gate valves

The following properties are used:
*   `VVAG_ITLck_HW_1` defines the device name that is used as the HW:1 trigger source
*   `VVAG_ITLck_HW_2` defines the device name that is used as the HW:2 trigger source
*   `VVAG_ITLck_HW_3` defines the device name that is used as the HW:3 trigger source
*   `VVAG_ITLck_Prs_1` defines the device name that is used as the Pressure:1 trigger source
*   `VVAG_ITLck_Prs_2` defines the device name that is used as the Pressure:2 trigger source
*   `VVAG_ITLck_Open` defines the device name that is used as the Open trigger source

If a trigger is not used then set the property to `-` (or the empty string once CCDB supports that)

## VACUUM_VAC-VVF.def

Contains PVs for variables coming from the PLC for the fast (sector) gate valves

The following properties are used:
*   `VVF_ITLck_Prev_1` defines the device name that is used as the Prev:1 trigger source
*   `VVF_ITLck_Prev_2` defines the device name that is used as the Prev:2 trigger source
*   `VVF_ITLck_Next_1` defines the device name that is used as the Next:1 trigger source
*   `VVF_ITLck_Next_2` defines the device name that is used as the Next:2 trigger source

If a trigger is not used then set the property to `-` (or the empty string once CCDB supports that)

## VACUUM_VAC-VVS.def

Contains PVs for variables coming from the PLC for the sector gate valves

The following properties are used:
*   `VVS_ITLck_Prev_1` defines the device name that is used as the Prev:1 trigger source
*   `VVS_ITLck_Prev_2` defines the device name that is used as the Prev:2 trigger source
*   `VVS_ITLck_Next_1` defines the device name that is used as the Next:1 trigger source
*   `VVS_ITLck_Next_2` defines the device name that is used as the Next:2 trigger source

If a trigger is not used then set the property to `-` (or the empty string once CCDB supports that)
