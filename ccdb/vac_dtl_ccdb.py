#!/usr/bin/env python

import os
import sys

thisdir = os.path.abspath(os.path.dirname(__file__))
sys.path.append(thisdir)

import vac_factory_helpers


epi_wc = os.path.dirname(thisdir)
epi_wc = None
HOST = "vacs-accv-vm-ioc"
EPICS_VERSION = vac_factory_helpers.VacuumFactory.EPICS_VERSION
E3_REQUIRE_VERSION = vac_factory_helpers.VacuumFactory.E3_REQUIRE_VERSION


def initialize_factory(factory = None):
    if factory is None:
        factory = vac_factory_helpers.VacuumFactory(epi_wc)

    factory.add_gauge_devtypes()
    factory.add_turbopump_devtypes()
    factory.add_ionpump_devtypes()

    factory.dtl_moxa_1 = "moxa-vac-dtl-1.tn.esss.lu.se"
    factory.dtl_moxa_2 = "moxa-vac-dtl-2.tn.esss.lu.se"

    return factory


########################
#
# Gauge IOCs
#
########################

def create_dtl010_mks_ioc(factory, host = HOST):
    dtl_moxa_1 = factory.dtl_moxa_1

    #
    # Adding IOC
    #
    ioc = factory.add_ioc("VacS-DTL:SC-IOC-110", host, description="IOC for DTL-010 vacuum gauge controllers and gauges")

    #
    # Adding device DTL-010:Vac-VEG-10001 of type VACUUM_VAC-VEG|VAC-VEVMC
    #
    mks = factory.add_mks(ioc, "DTL-010:Vac-VEG-10001", dtl_moxa_1, 4001, BOARD_A_SERIAL_NUMBER = "1902130826", BOARD_B_SERIAL_NUMBER = "1812101001", BOARD_C_SERIAL_NUMBER = "1812101017")
    factory.add_mks_vgp(mks, "DTL-010:Vac-VGP-10000", "A1")
    factory.add_mks_vgc(mks, "DTL-010:Vac-VGC-10000", "B1")
    factory.add_mks_vgc(mks, "DTL-010:Vac-VGC-50000", "C1")

    return ioc


def create_dtl020_mks_ioc(factory, host = HOST):
    dtl_moxa_1 = factory.dtl_moxa_1

    #
    # Adding IOC
    #
    ioc = factory.add_ioc("VacS-DTL:SC-IOC-210", host, description="IOC for DTL-020 vacuum gauge controllers and gauges")

    #
    # Adding device DTL-020:Vac-VEG-10001 of type VACUUM_VAC-VEG|VAC-VEVMC
    #
    mks = factory.add_mks(ioc, "DTL-020:Vac-VEG-10001", dtl_moxa_1, 4002, BOARD_A_SERIAL_NUMBER = "1807170621", BOARD_B_SERIAL_NUMBER = "1505060816", BOARD_C_SERIAL_NUMBER = "1505060842")
    factory.add_mks_vgp(mks, "DTL-020:Vac-VGP-10000", "A1")
    factory.add_mks_vgc(mks, "DTL-020:Vac-VGC-10000", "B1")
    factory.add_mks_vgc(mks, "DTL-020:Vac-VGC-50000", "C1")

    return ioc


def create_dtl030_mks_ioc(factory, host = HOST):
    dtl_moxa_1 = factory.dtl_moxa_1

    #
    # Adding IOC
    #
    ioc = factory.add_ioc("VacS-DTL:SC-IOC-310", host, description="IOC for DTL-030 vacuum gauge controllers and gauges")

    #
    # Adding device DTL-030:Vac-VEG-10001 of type VACUUM_VAC-VEG|VAC-VEVMC
    #
    mks = factory.add_mks(ioc, "DTL-030:Vac-VEG-10001", dtl_moxa_1, 4003, BOARD_A_SERIAL_NUMBER = "1804110710", BOARD_B_SERIAL_NUMBER = "1708241021", BOARD_C_SERIAL_NUMBER = "1707271218")
    factory.add_mks_vgp(mks, "DTL-030:Vac-VGP-10000", "A1")
    factory.add_mks_vgc(mks, "DTL-030:Vac-VGC-10000", "B1")
    factory.add_mks_vgc(mks, "DTL-030:Vac-VGC-50000", "C1")

    return ioc


def create_dtl040_mks_ioc(factory, host = HOST):
    dtl_moxa_1 = factory.dtl_moxa_1

    #
    # Adding IOC
    #
    ioc = factory.add_ioc("VacS-DTL:SC-IOC-410", host, description="IOC for DTL-040 vacuum gauge controllers and gauges")

    #
    # Adding device DTL-040:Vac-VEG-10001 of type VACUUM_VAC-VEG|VAC-VEVMC
    #
    mks = factory.add_mks(ioc, "DTL-040:Vac-VEG-10001", dtl_moxa_1, 4004, BOARD_A_SERIAL_NUMBER = "1902131001", BOARD_B_SERIAL_NUMBER = "1812100816", BOARD_C_SERIAL_NUMBER = "1812101009")
    factory.add_mks_vgp(mks, "DTL-040:Vac-VGP-10000", "A1")
    factory.add_mks_vgc(mks, "DTL-040:Vac-VGC-10000", "B1")
    factory.add_mks_vgc(mks, "DTL-040:Vac-VGC-50000", "C1")

    return ioc


def create_dtl050_mks_ioc(factory, host = HOST):
    dtl_moxa_1 = factory.dtl_moxa_1

    #
    # Adding IOC
    #
    ioc = factory.add_ioc("VacS-DTL:SC-IOC-510", host, description="IOC for DTL-050 vacuum gauge controllers and gauges")

    #
    # Adding device DTL-050:Vac-VEG-10001 of type VACUUM_VAC-VEG|VAC-VEVMC
    #
    mks = factory.add_mks(ioc, "DTL-050:Vac-VEG-10001", dtl_moxa_1, 4005, BOARD_A_SERIAL_NUMBER = "1902130807", BOARD_B_SERIAL_NUMBER = "1812100804", BOARD_C_SERIAL_NUMBER = "1812100953")
    factory.add_mks_vgp(mks, "DTL-050:Vac-VGP-10000", "A1")
    factory.add_mks_vgc(mks, "DTL-050:Vac-VGC-10000", "B1")
    factory.add_mks_vgc(mks, "DTL-050:Vac-VGC-50000", "C1")

    return ioc


def create_dtlx_mks_ioc(factory, host = HOST):
    dtl_moxa_1 = factory.dtl_moxa_1

    #
    # Adding IOC
    #
    ioc = factory.add_ioc("VacS-DTL:SC-IOC-010", host, description="IOC for cross-DTL vacuum gauge controllers and gauges")

    #
    # Adding device DTL-020:Vac-VEG-20001 of type VACUUM_VAC-VEG|VAC-VEVMC
    #
    mks = factory.add_mks(ioc, "DTL-020:Vac-VEG-20001", dtl_moxa_1, 4006, BOARD_A_SERIAL_NUMBER = "1902131341", BOARD_B_SERIAL_NUMBER = "1812100720", BOARD_C_SERIAL_NUMBER = "1812100822")
    factory.add_mks_vgp(mks, "DTL-030:Vac-VGP-00021", "A1")
    factory.add_mks_vgc(mks, "DTL-020:Vac-VGC-20000", "B1")
    factory.add_mks_vgc(mks, "DTL-030:Vac-VGC-20000", "C1")

    #
    # Adding device DTL-040:Vac-VEG-20001 of type VACUUM_VAC-VEG|VAC-VEVMC
    #
    mks = factory.add_mks(ioc, "DTL-040:Vac-VEG-20001", dtl_moxa_1, 4007, BOARD_A_SERIAL_NUMBER = "1609080843", BOARD_B_SERIAL_NUMBER = "1812101217", BOARD_C_SERIAL_NUMBER = "1812100930")
    factory.add_mks_vgc(mks, "DTL-010:Vac-VGC-20000", "A1")
    factory.add_mks_vgc(mks, "DTL-040:Vac-VGC-20000", "B1")
    factory.add_mks_vgc(mks, "DTL-050:Vac-VGC-20000", "C1")

    return ioc


def create_dtl_mks_iocs(factory, host = HOST):
    iocs = []
    iocs.append(create_dtl010_mks_ioc(factory, host))
    iocs.append(create_dtl020_mks_ioc(factory, host))
    iocs.append(create_dtl030_mks_ioc(factory, host))
    iocs.append(create_dtl040_mks_ioc(factory, host))
    iocs.append(create_dtl050_mks_ioc(factory, host))
    iocs.append(create_dtlx_mks_ioc(factory, host))

    return iocs


########################
#
# Ion-pump IOCs
#
########################

def create_dtl010_digitel_ioc(factory, host = HOST):
    dtl_moxa_2 = factory.dtl_moxa_2

    #
    # Adding IOC
    #
    ioc = factory.add_ioc("VacS-DTL:SC-IOC-140", host, description="IOC for DTL-010 vacuum ion pumps")

    #
    # Adding device DTL-010:Vac-VEPI-10001 of type VACUUM_VAC-VEPI
    #
    digitel = factory.add_digitel(ioc, "DTL-010:Vac-VEPI-10001", dtl_moxa_2, 4001)
    factory.add_digitel_pump_vpn(digitel, "DTL-010:Vac-VPN-10000", 1)
    factory.add_digitel_pump_vpn(digitel, "DTL-010:Vac-VPN-20000", 2)
    factory.add_digitel_pump_vpn(digitel, "DTL-010:Vac-VPN-30000", 3)
    factory.add_digitel_pump_vpn(digitel, "DTL-010:Vac-VPN-40000", 4)

    return ioc


def create_dtl020_digitel_ioc(factory, host = HOST):
    dtl_moxa_2 = factory.dtl_moxa_2

    #
    # Adding IOC
    #
    ioc = factory.add_ioc("VacS-DTL:SC-IOC-240", host, description="IOC for DTL-020 vacuum ion pumps")

    #
    # Adding device DTL-020:Vac-VEPI-10001 of type VACUUM_VAC-VEPI
    #
    digitel = factory.add_digitel(ioc, "DTL-020:Vac-VEPI-10001", dtl_moxa_2, 4002)
    factory.add_digitel_pump_vpn(digitel, "DTL-020:Vac-VPN-10000", 1)
    factory.add_digitel_pump_vpn(digitel, "DTL-020:Vac-VPN-20000", 2)
    factory.add_digitel_pump_vpn(digitel, "DTL-020:Vac-VPN-30000", 3)
    factory.add_digitel_pump_vpn(digitel, "DTL-020:Vac-VPN-40000", 4)

    return ioc


def create_dtl030_digitel_ioc(factory, host = HOST):
    dtl_moxa_2 = factory.dtl_moxa_2

    #
    # Adding IOC
    #
    ioc = factory.add_ioc("VacS-DTL:SC-IOC-340", host, description="IOC for DTL-030 vacuum ion pumps")

    #
    # Adding device DTL-030:Vac-VEPI-10001 of type VACUUM_VAC-VEPI
    #
    digitel = factory.add_digitel(ioc, "DTL-030:Vac-VEPI-10001", dtl_moxa_2, 4003)
    factory.add_digitel_pump_vpn(digitel, "DTL-030:Vac-VPN-10000", 1)
    factory.add_digitel_pump_vpn(digitel, "DTL-030:Vac-VPN-20000", 2)
    factory.add_digitel_pump_vpn(digitel, "DTL-030:Vac-VPN-30000", 3)
    factory.add_digitel_pump_vpn(digitel, "DTL-030:Vac-VPN-40000", 4)

    return ioc


def create_dtl040_digitel_ioc(factory, host = HOST):
    dtl_moxa_2 = factory.dtl_moxa_2

    #
    # Adding IOC
    #
    ioc = factory.add_ioc("VacS-DTL:SC-IOC-440", host, description="IOC for DTL-040 vacuum ion pumps")

    #
    # Adding device DTL-040:Vac-VEPI-10001 of type VACUUM_VAC-VEPI
    #
    digitel = factory.add_digitel(ioc, "DTL-040:Vac-VEPI-10001", dtl_moxa_2, 4004)
    factory.add_digitel_pump_vpn(digitel, "DTL-040:Vac-VPN-10000", 1)
    factory.add_digitel_pump_vpn(digitel, "DTL-040:Vac-VPN-20000", 2)
    factory.add_digitel_pump_vpn(digitel, "DTL-040:Vac-VPN-30000", 3)
    factory.add_digitel_pump_vpn(digitel, "DTL-040:Vac-VPN-40000", 4)

    return ioc


def create_dtl050_digitel_ioc(factory, host = HOST):
    dtl_moxa_2 = factory.dtl_moxa_2

    #
    # Adding IOC
    #
    ioc = factory.add_ioc("VacS-DTL:SC-IOC-540", host, description="IOC for DTL-050 vacuum ion pumps")

    #
    # Adding device DTL-050:Vac-VEPI-10001 of type VACUUM_VAC-VEPI
    #
    digitel = factory.add_digitel(ioc, "DTL-050:Vac-VEPI-10001", dtl_moxa_2, 4005)
    factory.add_digitel_pump_vpn(digitel, "DTL-050:Vac-VPN-10000", 1)
    factory.add_digitel_pump_vpn(digitel, "DTL-050:Vac-VPN-20000", 2)
    factory.add_digitel_pump_vpn(digitel, "DTL-050:Vac-VPN-30000", 3)
    factory.add_digitel_pump_vpn(digitel, "DTL-050:Vac-VPN-40000", 4)

    return ioc


def create_dtl_digitel_iocs(factory, host = HOST):
    iocs = []
    iocs.append(create_dtl010_digitel_ioc(factory, host))
    iocs.append(create_dtl020_digitel_ioc(factory, host))
    iocs.append(create_dtl030_digitel_ioc(factory, host))
    iocs.append(create_dtl040_digitel_ioc(factory, host))
    iocs.append(create_dtl050_digitel_ioc(factory, host))

    return iocs


########################
#
# Turbo pump IOCs
#
########################

def create_dtl010_tcp350_ioc(factory, host = HOST):
    dtl_moxa_2 = factory.dtl_moxa_2

    #
    # Adding IOC
    #
    ioc = factory.add_ioc("VacS-DTL:SC-IOC-130", host, description="IOC for DTL-010 vacuum turbopumps")

    #
    # Adding device DTL-010:Vac-VEPT-01100 of type VACUUM_VAC-VEPT
    #
    tcp = factory.add_tcp350(ioc, "DTL-010:Vac-VEPT-01100", dtl_moxa_2, 4006)
    factory.add_tcp_pump(tcp, "DTL-010:Vac-VPT-01100")

    return ioc


def create_dtl020_tcp350_ioc(factory, host = HOST):
    dtl_moxa_2 = factory.dtl_moxa_2

    #
    # Adding IOC
    #
    ioc = factory.add_ioc("VacS-DTL:SC-IOC-230", host, description="IOC for DTL-020 vacuum turbopumps")

    #
    # Adding device DTL-020:Vac-VEPT-01100 of type VACUUM_VAC-VEPT
    #
    tcp = factory.add_tcp350(ioc, "DTL-020:Vac-VEPT-01100", dtl_moxa_2, 4007)
    factory.add_tcp_pump(tcp, "DTL-020:Vac-VPT-01100")

    return ioc


def create_dtl030_tcp350_ioc(factory, host = HOST):
    dtl_moxa_2 = factory.dtl_moxa_2

    #
    # Adding IOC
    #
    ioc = factory.add_ioc("VacS-DTL:SC-IOC-330", host, description="IOC for DTL-030 vacuum turbopumps")

    #
    # Adding device DTL-030:Vac-VEPT-01100 of type VACUUM_VAC-VEPT
    #
    tcp = factory.add_tcp350(ioc, "DTL-030:Vac-VEPT-01100", dtl_moxa_2, 4008)
    factory.add_tcp_pump(tcp, "DTL-030:Vac-VPT-01100")

    return ioc


def create_dtl040_tcp350_ioc(factory, host = HOST):
    dtl_moxa_2 = factory.dtl_moxa_2

    #
    # Adding IOC
    #
    ioc = factory.add_ioc("VacS-DTL:SC-IOC-430", host, description="IOC for DTL-040 vacuum turbopumps")

    #
    # Adding device DTL-040:Vac-VEPT-01100 of type VACUUM_VAC-VEPT
    #
    tcp = factory.add_tcp350(ioc, "DTL-040:Vac-VEPT-01100", dtl_moxa_2, 4009)
    factory.add_tcp_pump(tcp, "DTL-040:Vac-VPT-01100")

    return ioc


def create_dtl050_tcp350_ioc(factory, host = HOST):
    dtl_moxa_2 = factory.dtl_moxa_2

    #
    # Adding IOC
    #
    ioc = factory.add_ioc("VacS-DTL:SC-IOC-530", host, description="IOC for DTL-050 vacuum turbopumps")

    #
    # Adding device DTL-050:Vac-VEPT-01100 of type VACUUM_VAC-VEPT
    #
    tcp = factory.add_tcp350(ioc, "DTL-050:Vac-VEPT-01100", dtl_moxa_2, 4010)
    factory.add_tcp_pump(tcp, "DTL-050:Vac-VPT-01100")

    return ioc


def create_dtl_tcp350_iocs(factory, host = HOST):
    iocs = []
    iocs.append(create_dtl010_tcp350_ioc(factory, host))
    iocs.append(create_dtl020_tcp350_ioc(factory, host))
    iocs.append(create_dtl030_tcp350_ioc(factory, host))
    iocs.append(create_dtl040_tcp350_ioc(factory, host))
    iocs.append(create_dtl050_tcp350_ioc(factory, host))

    return iocs


DTL_IOC = "VacS-DTL:SC-IOC-000"
def create_dtl_ioc(factory, name = DTL_IOC, host = HOST):
    #
    # Adding ioc of type IOC
    #
    ioc = factory.add_ioc(name, host)

    create_dtl_mks_iocs(factory,  host)
    create_dtl_tcp350_iocs(factory, host)
    create_dtl_digitel_iocs(factory, host)

    return ioc


create_ioc = create_dtl_ioc



if __name__ == "__main__":
    #
    # Create factory
    #
    factory = initialize_factory()

    main_ioc = factory.add_ioc("VacS-DTL")

    #
    # Create Gauge IOC
    #
    ioc = create_dtl_mks_iocs(factory)
    main_ioc.setControls(ioc)

    #
    # Create Turbo-pump IOC
    #
    ioc = create_dtl_tcp350_iocs(factory)
    main_ioc.setControls(ioc)

    #
    # Create Ion-pump IOC
    #
    ioc = create_dtl_digitel_iocs(factory)
    main_ioc.setControls(ioc)

    #
    # Saving the created CCDB
    #
    factory.save("VacS-DTL:SC-IOC-all")
