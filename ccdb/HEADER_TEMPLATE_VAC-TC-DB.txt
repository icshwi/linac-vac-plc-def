#FILENAME [PLCF#ext.to_filename("INSTALLATION_SLOT".lower().replace('-', '_'))]_tc-[PLCF#TIMESTAMP].substitutions
file [PLCF#ext.to_filename("INSTALLATION_SLOT".lower().replace('-', '_'))]_tc.template {
    pattern {COUPLER,         TC,                   ECATIO_MASTER_ID,  ECATIO_SLAVE_ID,  ECATIO_HW_TYPE,  ECATIO_CHANNEL_NUMBER}
