#!/usr/bin/env python

import os
import sys

thisdir = os.path.abspath(os.path.dirname(__file__))
sys.path.append(thisdir)

import vac_factory_helpers


epi_wc = os.path.dirname(thisdir)
epi_wc = None
HOST = "vacs-accv-vm-ioc"
EPICS_VERSION = vac_factory_helpers.VacuumFactory.EPICS_VERSION
E3_REQUIRE_VERSION = vac_factory_helpers.VacuumFactory.E3_REQUIRE_VERSION


def initialize_factory(factory = None):
    if factory is None:
        factory = vac_factory_helpers.VacuumFactory(epi_wc)

    factory.add_gauge_devtypes()
    factory.add_turbopump_devtypes()
    factory.add_ionpump_devtypes()

    factory.mebt_moxa = "moxa-vac-mebt.tn.esss.lu.se"

    return factory


GAUGE_IOC = "VacS-MEBT:SC-IOC-110"
def create_mebt_gauge_ioc(factory, name = GAUGE_IOC, host = HOST):
    mebt_moxa = factory.mebt_moxa

    #
    # Adding IOC
    #
    ioc = factory.add_ioc(name, host, description="IOC for MEBT vacuum gauge controllers and gauges")

    #
    # Adding device MEBT-010:Vac-VEG-01100
    #
    mks = factory.add_mks(ioc, "MEBT-010:Vac-VEG-01100", mebt_moxa, 4001, BOARD_A_SERIAL_NUMBER = "1807170606", BOARD_B_SERIAL_NUMBER = "1812040755", BOARD_C_SERIAL_NUMBER = "1812040805")
    factory.add_mks_vgp(mks, "MEBT-010:Vac-VGP-01100", "A1")

    #
    # Adding device MEBT-010:Vac-VEG-10001
    #
    mks = factory.add_mks(ioc, "MEBT-010:Vac-VEG-10001", mebt_moxa, 4002, BOARD_A_SERIAL_NUMBER = "1902131337", BOARD_B_SERIAL_NUMBER = "1812100924", BOARD_C_SERIAL_NUMBER = "1812100728")
    factory.add_mks_vgp(mks, "MEBT-010:Vac-VGP-20000", "A1")
    factory.add_mks_vgc(mks, "MEBT-010:Vac-VGC-10000", "B1")
    factory.add_mks_vgc(mks, "MEBT-010:Vac-VGC-30000", "C1")

    #
    # Adding device MEBT-010:Vac-VEG-10002
    #
    mks = factory.add_mks(ioc, "MEBT-010:Vac-VEG-10002", mebt_moxa, 4003, BOARD_A_SERIAL_NUMBER = "1902131009", BOARD_B_SERIAL_NUMBER = "1812100811", BOARD_C_SERIAL_NUMBER = "1812100748")
    factory.add_mks_vgc(mks, "MEBT-010:Vac-VGC-40000", "B1")

    return ioc


TCP_IOC = "VacS-MEBT:SC-IOC-130"
def create_mebt_turbopump_ioc(factory, name = TCP_IOC, host = HOST):
    mebt_moxa = factory.mebt_moxa

    #
    # Adding IOC
    #
    ioc = factory.add_ioc(name, host, description="IOC for MEBT vacuum turbopumps")

    #
    # Adding device MEBT-010:Vac-VEPT-01100
    #
    tcp = factory.add_tcp350(ioc, "MEBT-010:Vac-VEPT-01100", mebt_moxa, 4007)
    factory.add_tcp_pump(tcp, "MEBT-010:Vac-VPT-01100")

    return ioc


DIGITEL_IOC = "VacS-MEBT:SC-IOC-140"
def create_mebt_ionpump_ioc(factory, name = DIGITEL_IOC, host = HOST):
    mebt_moxa = factory.mebt_moxa

    #
    # Adding IOC
    #
    ioc = factory.add_ioc(name, host, description="IOC for MEBT vacuum ion pumps")

    #
    # Adding device MEBT-010:Vac-VEPI-10001
    #
    digitel = factory.add_digitel(ioc, "MEBT-010:Vac-VEPI-10001", mebt_moxa, 4005)
    factory.add_digitel_pump_vpi(digitel, "MEBT-010:Vac-VPI-10000", "1")
    factory.add_digitel_pump_vpi(digitel, "MEBT-010:Vac-VPI-20000", "2")
    factory.add_digitel_pump_vpi(digitel, "MEBT-010:Vac-VPI-30000", "3")
    factory.add_digitel_pump_vpi(digitel, "MEBT-010:Vac-VPI-40000", "4")

    #
    # Adding device MEBT-010:Vac-VEPI-20001
    #
    digitel = factory.add_digitel(ioc, "MEBT-010:Vac-VEPI-20001", mebt_moxa, 4006)
    factory.add_digitel_pump_vpi(digitel, "MEBT-010:Vac-VPI-50000", "1")

    return ioc


MEBT_IOC = "VacS-MEBT:SC-IOC-100"
def create_mebt_ioc(factory, name = MEBT_IOC, host = HOST):
    #
    # Adding IOC
    #
    ioc = factory.add_ioc(name, host)

    create_mebt_gauge_ioc(factory, ioc, host)
    create_mebt_turbopump_ioc(factory, ioc, host)
    create_mebt_ionpump_ioc(factory, ioc, host)

    return ioc


create_ioc = create_mebt_ioc



if __name__ == "__main__":
    #
    # Create factory
    #
    factory = initialize_factory()

    main_ioc = factory.add_ioc("VacS-MEBT")

    #
    # Create Gauge IOC
    #
    ioc = create_mebt_gauge_ioc(factory)
    main_ioc.setControls(ioc)

    #
    # Create Turbopump IOC
    #
    ioc = create_mebt_turbopump_ioc(factory)
    main_ioc.setControls(ioc)

    #
    # Create Ionpump IOC
    #
    ioc = create_mebt_ionpump_ioc(factory)
    main_ioc.setControls(ioc)

    #
    # Saving the created CCDB
    #
    factory.save("VacS-MEBT:SC-IOC-all")
