#!/usr/bin/env python

import os
import sys

thisdir = os.path.abspath(os.path.dirname(__file__))
sys.path.append(thisdir)

import vac_factory_helpers


epi_wc = os.path.dirname(thisdir)
epi_wc = None
HOST = "vacs-accv-vm-ioc"
EPICS_VERSION = vac_factory_helpers.VacuumFactory.EPICS_VERSION
E3_REQUIRE_VERSION = vac_factory_helpers.VacuumFactory.E3_REQUIRE_VERSION


def initialize_factory(factory = None):
    if factory is None:
        factory = vac_factory_helpers.VacuumFactory(epi_wc)

    factory.add_plcio_devtypes(plc = True)
    factory.add_sectorvalve_devtypes(plc = True)

    return factory


def create_interlock_plc_ioc(factory, name = "VacS-ACCV:SC-IOC-101", host = HOST):
    #
    # Adding IOC
    #
    ioc = factory.add_ioc(name, host, description="IOC for Interlock PLC of High level Proton Beam Vacuum Control System", ioc_repo="https://gitlab.esss.lu.se/iocs/vacs/accv/vacs_accv_sc_ioc_101")

    #
    # Adding PLC: VacS-ACCV:Vac-PLC-10001
    #
    plc = ioc.addPLC("VacS-ACCV:Vac-PLC-10001")
    # Properties
    plc.setProperty("PLCF#PLC-EPICS-COMMS:Endianness", "BigEndian")
    plc.setProperty("PLCF#EPICSToPLCDataBlockStartOffset", "0")
    plc.setProperty("PLCF#PLCToEPICSDataBlockStartOffset", "0")
    plc.setProperty("PLCF#PLC-EPICS-COMMS: MBConnectionID", "255")
    plc.setProperty("PLCF#PLC-EPICS-COMMS: MBPort", "502")
    plc.setProperty("PLCF#PLC-EPICS-COMMS: S7ConnectionID", "256")
    plc.setProperty("PLCF#PLC-DIAG:Max-IO-Devices", "10")
    plc.setProperty("PLCF#PLC-EPICS-COMMS: S7Port", "2000")
    plc.setProperty("EPICSSnippet", [])
    plc.setProperty("PLCF#PLC-EPICS-COMMS: InterfaceID", "72")
    plc.setProperty("PLCF#PLC-DIAG:Max-Local-Modules", "10")
    plc.setProperty("EPICSModule", [])
    plc.setProperty("PLCF#PLC-EPICS-COMMS: PLCPulse", "Pulse_200ms")
    plc.setProperty("PLCF#PLC-EPICS-COMMS: DiagPort", "2001")
    plc.setProperty("PLCF#PLC-DIAG:Max-Modules-In-IO-Device", "20")
    plc.setProperty("PLCF#PLC-EPICS-COMMS: DiagConnectionID", "254")
    plc.setProperty("Hostname", "vac-plc-intlk.tn.esss.lu.se")
    # External links
    plc.addLink("EPI[PLC-itlck]", factory.git_repo(), epi_wc)
    plc.addLink("BEAST TEMPLATE[PLC-itlck]", factory.git_repo(), epi_wc)

    """
        LEBT
    """
    create_lebt_devices(factory, plc)

    """
        RFQ
    """
    create_rfq_devices(factory, plc)

    """
        MEBT
    """
    create_mebt_devices(factory, plc)

    """
        DTL
    """
    create_dtl_devices(factory, plc)

    return ioc


def create_lebt_devices(factory, plc):
    #
    # Adding device LEBT-010:Vac-PLCIO-10001
    #
    plcio = factory.add_plcio(plc, "LEBT-010:Vac-PLCIO-10001")

    #
    # Adding device LEBT-010:Vac-VVS-20000 of type VACUUM_VAC-VVS
    #
    factory.add_vvs(plcio, "LEBT-010:Vac-VVS-20000", Prev_1 = "LEBT-010:Vac-VGC-10000", Prev_2 = "LEBT-010:Vac-VPG-10001", Next_1 = "LEBT-010:Vac-VGC-30000", Next_2 = "LEBT-010:Vac-VPG-20001")

    #
    # Adding device LEBT-010:Vac-VVS-40000 of type VACUUM_VAC-VVS
    #
    factory.add_vvs(plcio, "LEBT-010:Vac-VVS-40000", Prev_1 = "LEBT-010:Vac-VGC-30000", Prev_2 = "LEBT-010:Vac-VPG-20001", Next_1 = "RFQ-010:Vac-VGC-10000", Next_2 = "RFQ-010:Vac-VGC-40000")


def create_rfq_devices(factory, plc):
    #
    # Adding device RFQ-010:Vac-PLCIO-10002
    #
    plcio = factory.add_plcio(plc, "RFQ-010:Vac-PLCIO-10002")

    #
    # Adding device RFQ-010:Vac-PLCIO-10003
    #
    plcio = factory.add_plcio(plc, "RFQ-010:Vac-PLCIO-10003")

    #
    # Adding device RFQ-010:Vac-VVS-10000 of type VACUUM_VAC-VVS
    #
    factory.add_vvs(plcio, "RFQ-010:Vac-VVS-10000", Prev_1 = "RFQ-010:Vac-VGC-30000", Prev_2 = "RFQ-010:Vac-VGC-40000")


def create_mebt_devices(factory, plc):
    #
    # Adding device MEBT-010:Vac-PLCIO-10004
    #
    plcio = factory.add_plcio(plc, "MEBT-010:Vac-PLCIO-10004")


def create_dtl_devices(factory, plc):
    #
    # Adding device DTL-010:Vac-PLCIO-10005
    #
    plcio = factory.add_plcio(plc, "DTL-010:Vac-PLCIO-10005")

    #
    # Adding device DTL-010:Vac-PLCIO-10006
    #
    plcio = factory.add_plcio(plc, "DTL-010:Vac-PLCIO-10006")

    #
    # Adding device DTL-010:Vac-VVS-10000 of type VACUUM_VAC-VVS
    #
    factory.add_vvs(plc, "DTL-010:Vac-VVS-10000", Prev_1 = "MEBT-010:Vac-VGC-40000", Prev_2 = "MEBT-010:Vac-VPI-40000", Next_1 = "DTL-010:Vac-VGC-10000", Next_2 = "DTL-010:Vac-VPN-10000")

    #
    # Adding device DTL-020:Vac-VVS-10000 of type VACUUM_VAC-VVS
    #
    factory.add_vvs(plc, "DTL-020:Vac-VVS-10000")

    """
    #
    # Adding device DTL-030:Vac-VVS-10000 of type VACUUM_VAC-VVS
    #
    factory.add_vvs(plc, "DTL-030:Vac-VVS-10000")

    #
    # Adding device DTL-040:Vac-VVS-10000 of type VACUUM_VAC-VVS
    #
    factory.add_vvs(plc, "DTL-040:Vac-VVS-10000")

    #
    # Adding device DTL-050:Vac-VVS-10000 of type VACUUM_VAC-VVS
    #
    factory.add_vvs(plc, "DTL-050:Vac-VVS-10000")
    """


create_ioc = create_interlock_plc_ioc



if __name__ == "__main__":
    #
    # Create factory
    #
    factory = initialize_factory()

    #
    # Create IOC
    #
    ioc = create_interlock_plc_ioc(factory)

    #
    # Saving the created CCDB
    #
    factory.save(ioc.name())
