#!/usr/bin/env python

import os
import sys

sys.path.append(os.path.curdir)
thisdir = os.path.abspath(os.path.dirname(__file__))
sys.path.append(thisdir)

if __name__ == "__main__":
    import vac_process_plc_ccdb
    import vac_interlock_plc_ccdb
    import vac_lebt_ccdb
    import vac_rfq_ccdb
    import vac_mebt_ccdb
    import vac_dtl_ccdb
    import vac_ts2_ccdb

    #
    # Create factory
    #
    factory = None
    factory = vac_process_plc_ccdb.initialize_factory(factory)
    factory = vac_interlock_plc_ccdb.initialize_factory(factory)
    factory = vac_lebt_ccdb.initialize_factory(factory)
    factory = vac_rfq_ccdb.initialize_factory(factory)
    factory = vac_mebt_ccdb.initialize_factory(factory)
    factory = vac_dtl_ccdb.initialize_factory(factory)
    factory = vac_ts2_ccdb.initialize_factory(factory)

    vac_process_plc_ccdb.create_ioc(factory)
    vac_interlock_plc_ccdb.create_ioc(factory)
    vac_lebt_ccdb.create_ioc(factory)
    vac_rfq_ccdb.create_ioc(factory)
    vac_mebt_ccdb.create_ioc(factory)
    vac_dtl_ccdb.create_ioc(factory)
    vac_ts2_ccdb.create_ioc(factory)

    #
    # Saving the created CCDB
    #
    factory.save("vacuum")
