import os
import helpers


class VacuumFactory(object):
    # IOC related versions
    EPICS_VERSION = "7.0.5"
    E3_REQUIRE_VERSION = "3.4.1"

    # Module versions
    MKS_MODULE_VERSION = "4.5.0"
    TPG_MODULE_VERSION = None
    LEYBOLD_MODULE_VERSION = "3.5.0"
    TCP_MODULE_VERSION = "1.5.0"
    DIGITEL_MODULE_VERSION = None

    # Device types
    DEVTYPE_GAUGE_VGC = "VACUUM_VAC-VGC"
    DEVTYPE_GAUGE_VGD = "VACUUM_VAC-VGD"
    DEVTYPE_GAUGE_VGP = "VACUUM_VAC-VGP"
    DEVTYPE_GAUGE_VVMC = "VACUUM_VAC-VVMC"
    DEVTYPE_CONTROLLER_GAUGE = "VACUUM_VAC-VEG|VAC-VEVMC"

    DEVTYPE_VERA = "VACUUM_VAC-VERA"
    DEVTYPE_VGR = "VACUUM_VAC-VGR"

    DEVTYPE_PUMPING_GROUP = "VACUUM_VAC-VPG"

    DEVTYPE_PUMP_VPP = "VACUUM_VAC-VPP|VAC-VPDP"

    DEVTYPE_PUMP_VPT = "VACUUM_VAC-VPT"
    DEVTYPE_CONTROLLER_VPT = "VACUUM_VAC-VEPT"

    DEVTYPE_PUMP_VPI = "VACUUM_VAC-VPI"
    DEVTYPE_PUMP_VPN = "VACUUM_VAC-VPN"
    DEVTYPE_CONTROLLER_VPI = "VACUUM_VAC-VEPI"
    DEVTYPE_CONTROLLER_VPN = "VACUUM_VAC-VEPN"

    DEVTYPE_VALVE_VVA = "VACUUM_VAC-VVA|VAC-VVG"
    DEVTYPE_VALVE_VVG = "VACUUM_VAC-VVA|VAC-VVG"
    DEVTYPE_VALVE_VVA_BYPASS = "VACUUM_VAC-VVA|BYPASS|"
    DEVTYPE_VALVE_VVF = "VACUUM_VAC-VVF"
    DEVTYPE_VALVE_VVS = "VACUUM_VAC-VVS"

    DEVTYPE_PLCIO = "VACUUM_VAC-PLCIO"

    DEVTYPE_VPSU = "VACUUM_VAC-VPSU"


    def __init__(self, wc = None, epics_version = None, e3_require_version = None):
        super(VacuumFactory, self).__init__()

        self.__thisdir = os.path.abspath(os.path.dirname(__file__))
        self._git = "https://gitlab.esss.lu.se/icshwi/linac-vac-plc-def"
        self._wc = wc
        self.add_vvg = self.add_vva

        if epics_version:
            self.EPICS_VERSION = epics_version
        if e3_require_version:
            self.E3_REQUIRE_VERSION = e3_require_version

        from ccdb_factory import CCDB_Factory
        self._factory = CCDB_Factory()


    @staticmethod
    def devname_to_filename(name):
        return name.lower().replace('-', '_').replace(':', '_')


    @staticmethod
    def togit(*args):
        args = list(args)
        if not args[-1].endswith(".git"):
            args[-1] = ".".join([args[-1], "git"])

        return helpers.urljoin(*args)


    @staticmethod
    def _add_macros(dev, kwargs):
        ks = sorted(kwargs.keys())
        if "EPICSModuleVersion" in ks:
            k = "EPICSModuleVersion"
            dev.setProperty(k, [kwargs[k]])
            ks.remove(k)
        for k in ks:
            dev.setProperty("EPICSMacro#{}".format(k), kwargs[k])


    @staticmethod
    def ioc_git():
        return "https://gitlab.esss.lu.se/iocs/vacs"


    @staticmethod
    def ioc_repo(ioc):
        return VacuumFactory.togit(VacuumFactory.ioc_git(), VacuumFactory.devname_to_filename(ioc))


    def __artifactDir(self, filename):
        return os.path.join(self.__thisdir, filename)


    def _addArtifact(self, *args):
        self._factory.addArtifact(*args)


    def git_repo(self):
        return self._git


    def ccdb(self):
        return self._factory


    def save(self, fname):
        return self.ccdb().save(fname)


    def add_ioc(self, name, host = None, epics_version = None, e3_require_version = None, description = None, ioc_repo = None):
        if isinstance(name, str):
            ioc = self._factory.addDevice("IOC", name, description = description)
            if host:
                ioc.setProperty("Hostname", host)
                ioc.addLink("IOC_REPOSITORY", "{}".format(ioc_repo if ioc_repo else self.ioc_repo(name)), download = False)
                ioc.addLink("BEAST TREE[LINAC-VAC]", self._git, self._wc)
                ioc.setProperty("EPICSVersion", epics_version if epics_version else self.EPICS_VERSION)
                ioc.setProperty("E3RequireVersion", e3_require_version if e3_require_version else self.E3_REQUIRE_VERSION)
        else:
            ioc = name

        return ioc


    #
    # DEVTYPES
    #
    def add_gauge_devtypes(self, plc = False):
        # Artifacts for VACUUM_VAC-VGC / VGD / VGP
        self._factory.addArtifact(VacuumFactory.DEVTYPE_GAUGE_VGC, "TEMPLATE_ARCHIVE.txt", self.__artifactDir("TEMPLATE_VAC-GAUGE-ARCHIVE.txt"))
        self._factory.addArtifact(VacuumFactory.DEVTYPE_GAUGE_VGD, "TEMPLATE_ARCHIVE.txt", self.__artifactDir("TEMPLATE_VAC-GAUGE-ARCHIVE.txt"))
        self._factory.addArtifact(VacuumFactory.DEVTYPE_GAUGE_VGP, "TEMPLATE_ARCHIVE.txt", self.__artifactDir("TEMPLATE_VAC-GAUGE-ARCHIVE.txt"))

        # Artifacts for VACUUM_VAC-VVMC
        self._factory.addArtifact(VacuumFactory.DEVTYPE_GAUGE_VVMC, "TEMPLATE_ARCHIVE.txt", self.__artifactDir("TEMPLATE_VAC-MFC-ARCHIVE.txt"))

        # External links for VACUUM_VAC-VEG|VAC-VEVMC
        self._factory.addLink(VacuumFactory.DEVTYPE_CONTROLLER_GAUGE, "BEAST TEMPLATE[device]", self._git, self._wc)


    def add_pumping_group_devtypes(self, plc = True):
        # External links
        self._factory.addLink(VacuumFactory.DEVTYPE_PUMPING_GROUP, "EPI", self._git, self._wc)


    def add_primarypump_devtypes(self, plc = True):
        # External links
        self._factory.addLink(VacuumFactory.DEVTYPE_PUMP_VPP, "EPI", self._git, self._wc)
        self._factory.addLink(VacuumFactory.DEVTYPE_PUMP_VPP, "BEAST TEMPLATE[primary-pump]", self._git, self._wc)


    def add_turbopump_devtypes(self, plc = False):
        # Artifacts for controller
        self._factory.addArtifact(VacuumFactory.DEVTYPE_CONTROLLER_VPT, "TEMPLATE_ARCHIVE.txt", self.__artifactDir("TEMPLATE_VAC-VEPT-ARCHIVE.txt"))

        # External links for controller
        self._factory.addLink(VacuumFactory.DEVTYPE_CONTROLLER_VPT, "BEAST TEMPLATE[device]", self._git, self._wc)

        # External links for pump
        if plc:
            self._factory.addLink(VacuumFactory.DEVTYPE_PUMP_VPT, "EPI", self._git, self._wc)


    def add_ionpump_devtypes(self, plc = False):
        # Artifacts for VPI pumps
        self._factory.addArtifact(VacuumFactory.DEVTYPE_PUMP_VPI, "TEMPLATE_ARCHIVE.txt", self.__artifactDir("TEMPLATE_VAC-VPI-ARCHIVE.txt"))
        # External links for VPI pump
        if plc:
            self._factory.addLink(VacuumFactory.DEVTYPE_PUMP_VPI, "EPI", self._git, self._wc)

        # Artifacts for VPN pumps
        self._factory.addArtifact(VacuumFactory.DEVTYPE_PUMP_VPN, "TEMPLATE_ARCHIVE.txt", self.__artifactDir("TEMPLATE_VAC-VPN-ARCHIVE.txt"))
        # External links for VPN pumps
        if plc:
            self._factory.addLink(VacuumFactory.DEVTYPE_PUMP_VPN, "EPI[{}]".format(VacuumFactory.DEVTYPE_PUMP_VPI), self._git, self._wc)

        # External links for VPI controllers
        self._factory.addLink(VacuumFactory.DEVTYPE_CONTROLLER_VPI, "BEAST TEMPLATE[device]", self._git, self._wc)

        # External links for VPN controllers
        self._factory.addLink(VacuumFactory.DEVTYPE_CONTROLLER_VPN, "BEAST TEMPLATE[device]", self._git, self._wc)


    def add_valve_devtypes(self, plc = True):
        # Artifacts for VVA | VVG
#       self._factory.addArtifact(VacuumFactory.DEVTYPE_VALVE_VVA, self.__artifactDir("TEMPLATE_CAPUT-ITLCK-DEVICE.txt"))
#       self._factory.addArtifact(VacuumFactory.DEVTYPE_VALVE_VVA, self.__artifactDir("TEMPLATE_EPICS-DB-ITLCK-DEVICE.txt"))

        # External links for VVA | VVG
        self._factory.addLink(VacuumFactory.DEVTYPE_VALVE_VVA, "EPI", self._git, self._wc)

        # Artifacts for VVA_BYPASS
#       self._factory.addArtifact(VacuumFactory.DEVTYPE_VALVE_VVA_BYPASS, self.__artifactDir("TEMPLATE_CAPUT-ITLCK-DEVICE.txt"))
#       self._factory.addArtifact(VacuumFactory.DEVTYPE_VALVE_VVA_BYPASS, self.__artifactDir("TEMPLATE_EPICS-DB-ITLCK-DEVICE.txt"))

        # External links for VVA_BYPASS
        self._factory.addLink(VacuumFactory.DEVTYPE_VALVE_VVA_BYPASS, "EPI", self._git, self._wc)


    def add_sectorvalve_devtypes(self, plc = True):
        # Artifacts
        self._factory.addArtifact(VacuumFactory.DEVTYPE_VALVE_VVS, self.__artifactDir("TEMPLATE_CAPUT-ITLCK-DEVICE.txt"))

        # External links
        self._factory.addLink(VacuumFactory.DEVTYPE_VALVE_VVF, "EPI", self._git, self._wc)
        self._factory.addLink(VacuumFactory.DEVTYPE_VALVE_VVS, "EPI", self._git, self._wc)


    def add_plcio_devtypes(self, plc = True):
        # External links
        self._factory.addLink(VacuumFactory.DEVTYPE_PLCIO, "EPI", self._git, self._wc)


    def add_vpsu_devtypes(self, plc = True):
        # External links
        self._factory.addLink(VacuumFactory.DEVTYPE_VPSU, "EPI", self._git, self._wc)


    #
    # DEVICES
    #
    def add_switchyard(self, ioc, name, plc, gauge_1, gauge_2, **kwargs):
        #
        # Adding Starting Switchyard of type VACUUM_VAC-VPG
        #
        yard = ioc.addDevice(VacuumFactory.DEVTYPE_PUMPING_GROUP, name)
        # Properties
        yard.setProperty("EPICSSnippet", ['vac_starting_switchyard'])
        yard.setProperty("EPICSModule", ['vac_starting_switchyard'])
        kwargs["PLC"] = plc if isinstance(plc, str) else plc.name()
        kwargs["GAUGE_1"] = gauge_1 if isinstance(gauge_1, str) else gauge_1.name()
        kwargs["GAUGE_2"] = gauge_2 if isinstance(gauge_2, str) else gauge_2.name()
        _add_macros(yard, kwargs)

        return yard


    def add_vera(self, ioc, name, moxahost, moxaport, **kwargs):
        #
        # Adding Residual gas analyzer controller of type VACUUM_VAC-VERA
        #
        vera = ioc.addDevice(VacuumFactory.DEVTYPE_VERA, name)
        # Properties
        vera.setProperty("EPICSModule", [], "Strings List")
        vera.setProperty("EPICSSnippet", [], "Strings List")
        vera.setProperty("MOXAHostname", moxahost)
        vera.setProperty("MOXAPort", moxaport)
        kwargs["IPADDR"] = moxahost
        kwargs["PORT"] = moxaport
        _add_macros(vera, kwargs)

        return vera


    def add_rga(self, vera, name, moxahost, moxaport, **kwargs):
        #
        # Adding residual gas analyzer of type VACUUM_VAC-VGR
        #
        rga = vera.addDevice(VacuumFactory.DEVTYPE_VGR, name)
        # Properties
        rga.setProperty("EPICSModule", ['vac_ctrl_halrcrga'])
        rga.setProperty("VacController", vera.name())
        rga.setProperty("EPICSSnippet", ['vac_ctrl_halrcrga_moxa'])
        rga.setProperty("MOXAHostname", moxahost)
        rga.setProperty("MOXAPort", moxaport)
        kwargs["CONTROLLERNAME"] = vera.name()
        kwargs["IPADDR"] = moxahost
        kwargs["PORT"] = moxaport
        _add_macros(rga, kwargs)

        return rga


    def add_coupler(self, ioc, name, vac_section, ecatio_slave_id, ecatio_hw_type, plc, **kwargs):
        #
        # Adding EtherCAT coupler
        #
        coupler = ioc.addDevice("VACUUM_VAC-ECATIO-COUPLER", name)
        # Properties
        coupler.setProperty("PLCF#VAC_SECTION", vac_section)
        coupler.setProperty("PLCF#ECATIO_COUPLER", name)
        coupler.setProperty("PLCF#ECATIO_SLAVE_ID", ecatio_slave_id, int)
        coupler.setProperty("PLCF#ECATIO_HW_TYPE", ecatio_hw_type, str)
        coupler.setProperty("PLCF#ECATIO_PLC_IDX", plc.idx(), int)
        _add_macros(coupler, kwargs)

        return coupler


    def add_daq(self, coupler, name, ecatio_master_id, ecatio_slave_id, ecatio_hw_type, ecatio_channel, ds, **kwargs):
        #
        # Adding fast DAQ gauge
        #
        daq = coupler.addDevice("VACUUM_VAC-VG-DAQ", name)
        # Properties
        daq.setProperty("PLCF#ECATIO_MASTER_ID", ecatio_master_id, int)
        daq.setProperty("PLCF#ECATIO_SLAVE_ID", ecatio_slave_id, int)
        daq.setProperty("PLCF#ECATIO_HW_TYPE", ecatio_hw_type, str)
        daq.setProperty("PLCF#ECATIO_CHANNEL_NUMBER", ecatio_channel, int)
        daq.setProperty("PLCF#ECATIO_DS_IDX", ds.idx(), int)
        _add_macros(daq, kwargs)

        return daq


    def add_tc(self, coupler, name, ecatio_master_id, ecatio_slave_id, ecatio_hw_type, ecatio_channel, **kwargs):
        #
        # Adding TC thermocouple
        #
        daq = coupler.addDevice("VACUUM_VAC-TC", name)
        # Properties
        daq.setProperty("PLCF#ECATIO_MASTER_ID", ecatio_master_id, int)
        daq.setProperty("PLCF#ECATIO_SLAVE_ID", ecatio_slave_id, int)
        daq.setProperty("PLCF#ECATIO_HW_TYPE", ecatio_hw_type, str)
        daq.setProperty("PLCF#ECATIO_CHANNEL_NUMBER", ecatio_channel, int)
        _add_macros(daq, kwargs)

        return daq


    def add_mks(self, ioc, name, moxahost, moxaport, **kwargs):
        #
        # Adding MKS gauge controller of type DEVTYPE_CONTROLLER_GAUGE
        #
        mks = ioc.addDevice(VacuumFactory.DEVTYPE_CONTROLLER_GAUGE, name)
        # Properties
        mks.setProperty("MOXAHostname", moxahost)
        mks.setProperty("EPICSModule", ['vac_ctrl_mks946_937b'])
        if self.MKS_MODULE_VERSION:
            mks.setProperty("EPICSModuleVersion", [self.MKS_MODULE_VERSION])
        mks.setProperty("EPICSSnippet", ['vac_ctrl_mks946_937b_moxa'])
        mks.setProperty("MOXAPort", moxaport)
        kwargs["IPADDR"] = moxahost
        kwargs["PORT"] = moxaport
        _add_macros(mks, kwargs)

        return mks


    def add_mks_vgp(self, mks, name, channel, **kwargs):
        #
        # Adding MKS Pirani gauge of type DEVTYPE_GAUGE_VGP
        #
        mks_vgp = mks.addDevice(VacuumFactory.DEVTYPE_GAUGE_VGP, name)
        # Properties
        mks_vgp.setProperty("EPICSModule", ['vac_ctrl_mks946_937b'])
        mks_vgp.setProperty("EPICSSnippet", ['vac_gauge_mks_vgp'])
        mks_vgp.setProperty("VacController", mks.name())
        mks_vgp.setProperty("MKSChannel", channel)
        kwargs["CHANNEL"] = channel
        kwargs["CONTROLLERNAME"] = mks.name()
        _add_macros(mks_vgp, kwargs)

        return mks_vgp


    def add_mks_vgc(self, mks, name, channel, **kwargs):
        #
        # Adding MKS Cold cathode gauge of type DEVTYPE_GAUGE_VGC
        #
        mks_vgc = mks.addDevice(VacuumFactory.DEVTYPE_GAUGE_VGC, name)
        # Properties
        mks_vgc.setProperty("EPICSModule", ['vac_ctrl_mks946_937b'])
        mks_vgc.setProperty("EPICSSnippet", ['vac_gauge_mks_vgc'])
        mks_vgc.setProperty("VacController", mks.name())
        mks_vgc.setProperty("MKSChannel", channel)
        kwargs["CHANNEL"] = channel
        kwargs["CONTROLLERNAME"] = mks.name()
        _add_macros(mks_vgc, kwargs)

        return mks_vgc


    def add_mks_vgd(self, mks, name, channel, **kwargs):
        #
        # Adding MKS capacitance manomenter gauge of type DEVTYPE_GAUGE_VGD
        #
        mks_vgd = mks.addDevice(VacuumFactory.DEVTYPE_GAUGE_VGD, name)
        # Properties
        mks_vgd.setProperty("EPICSModule", ['vac_ctrl_mks946_937b'])
        mks_vgd.setProperty("EPICSSnippet", ['vac_gauge_mks_vgd'])
        mks_vgd.setProperty("VacController", mks.name())
        mks_vgd.setProperty("MKSChannel", channel)
        kwargs["CHANNEL"] = channel
        kwargs["CONTROLLERNAME"] = mks.name()
        _add_macros(mks_vgd, kwargs)

        return mks_vgd


    def add_mks_vvmc(self, mks, name, channel, **kwargs):
        #
        # Adding MKS mass flow meter gauge of type DEVTYPE_GAUGE_VVMC
        #
        mks_vvmc = mks.addDevice(VacuumFactory.DEVTYPE_GAUGE_VVMC, name)
        # Properties
        mks_vvmc.setProperty("EPICSModule", ['vac_ctrl_mks946_937b'])
        mks_vvmc.setProperty("EPICSSnippet", ['vac_mfc_mks_gv50a'])
        mks_vvmc.setProperty("VacController", mks.name())
        mks_vvmc.setProperty("MKSChannel", channel)
        kwargs["CHANNEL"] = channel
        kwargs["CONTROLLERNAME"] = mks.name()
        _add_macros(mks_vvmc, kwargs)

        return mks_vvmc


    def add_tpg(self, ioc, name, moxahost, moxaport, **kwargs):
        #
        # Adding TPG gauge controller of type DEVTYPE_CONTROLLER_GAUGE
        #
        tpg = ioc.addDevice(VacuumFactory.DEVTYPE_CONTROLLER_GAUGE, name)
        # Properties
        tpg.setProperty("MOXAHostname", moxahost)
        tpg.setProperty("EPICSModule", ['vac_ctrl_tpg300_500'])
        if self.TPG_MODULE_VERSION:
            tpg.setProperty("EPICSModuleVersion", [self.TPG_MODULE_VERSION])
        tpg.setProperty("EPICSSnippet", ['vac_ctrl_tpg300_500_moxa'])
        tpg.setProperty("MOXAPort", moxaport)
        kwargs["IPADDR"] = moxahost
        kwargs["PORT"] = moxaport
        _add_macros(tpg, kwargs)

        return tpg


    def add_tpg_vgc(self, tpg, name, channel, ecatio_slot = None, ecatio_channel = None, **kwargs):
        #
        # Adding TPG Cold cathode gauge of type DEVTYPE_GAUGE_VGC
        #
        tpg_vgc = tpg.addDevice(VacuumFactory.DEVTYPE_GAUGE_VGC, name)
        # Properties
        tpg_vgc.setProperty("EPICSModule", ['vac_ctrl_tpg300_500'])
        tpg_vgc.setProperty("EPICSSnippet", ['vac_gauge_tpg300_500_vgc'])
        tpg_vgc.setProperty("VacController", tpg.name())
        tpg_vgc.setProperty("TPGChannel", channel)
        tpg_vgc.setProperty("PLCF#ECATIOSlotNumber", ecatio_slot, int)
        tpg_vgc.setProperty("PLCF#ECATIOChannelNumber", ecatio_channel, int)
        kwargs["CHANNEL"] = channel
        kwargs["CONTROLLERNAME"] = tpg.name()
        _add_macros(tpg_vgc, kwargs)

        return tpg_vgc


    def add_leybold(self, ioc, name, moxahost, moxaport, **kwargs):
        #
        # Adding Leybold TD20 turbo pump controller
        #
        leybold = ioc.addDevice(VacuumFactory.DEVTYPE_CONTROLLER_VPT, name)
        # Properties
        leybold.setProperty("EPICSModule", ['vac_ctrl_leyboldtd20'])
        if self.LEYBOLD_MODULE_VERSION:
            leybold.setProperty("EPICSModuleVersion", [self.LEYBOLD_MODULE_VERSION])
        leybold.setProperty("EPICSSnippet", ['vac_ctrl_leyboldtd20_moxa'])
        leybold.setProperty("MOXAHostname", moxahost)
        leybold.setProperty("MOXAPort", moxaport)
        kwargs["IPADDR"] = moxahost
        kwargs["PORT"] = moxaport
#       kwargs["Hostname"] = moxahost
#       kwargs["PORT"] = moxaport
        _add_macros(leybold, kwargs)

        return leybold


    def add_leybold_pump(self, leybold, name, **kwargs):
        #
        # Adding Leybold TD20 turbo pump device
        #
        pump = leybold.addDevice(VacuumFactory.DEVTYPE_PUMP_VPT, name)
        # Properties
        pump.setProperty("EPICSModule", ['vac_ctrl_leyboldtd20'])
        pump.setProperty("EPICSSnippet", ['vac_pump_leyboldtd20_vpt'])
        kwargs["CONTROLLERNAME"] = leybold.name()
        _add_macros(pump, kwargs)

        return pump


    def add_digitel(self, ioc, name, moxahost, moxaport, **kwargs):
        #
        # Adding DigitelQpce ion pump controller as a VEPI
        #
        digitel = ioc.addDevice(VacuumFactory.DEVTYPE_CONTROLLER_VPI, name)
        # Properties
        digitel.setProperty("MOXAHostname", moxahost)
        digitel.setProperty("EPICSModule", ['vac_ctrl_digitelqpce'])
        if self.DIGITEL_MODULE_VERSION:
            digitel.setProperty("EPICSModuleVersion", [self.DIGITEL_MODULE_VERSION])
        digitel.setProperty("EPICSSnippet", ['vac_ctrl_digitelqpce_moxa'])
        digitel.setProperty("MOXAPort", moxaport)
        kwargs["IPADDR"] = moxahost
        kwargs["PORT"] = moxaport
        _add_macros(digitel, kwargs)

        return digitel


    def add_digitel_pump_vpi(self, digitel, name, channel, **kwargs):
        #
        # Adding DigitelQpce ion pump as a VPI
        #
        pump = digitel.addDevice(VacuumFactory.DEVTYPE_PUMP_VPI, name)
        # Properties
        pump.setProperty("EPICSModule", ['vac_ctrl_digitelqpce'])
        pump.setProperty("EPICSSnippet", ['vac_pump_digitelqpce_vpi'])
        pump.setProperty("VacController", digitel.name())
        pump.setProperty("QPCeChannel", channel)
        kwargs["CHANNEL"] = channel
        kwargs["CONTROLLERNAME"] = digitel.name()
        _add_macros(pump, kwargs)

        return pump


    def add_digitel_pump_vpn(self, digitel, name, channel, **kwargs):
        #
        # Adding DigitelQpce ion pump as a VPN
        #
        pump = digitel.addDevice(VacuumFactory.DEVTYPE_PUMP_VPN, name)
        # Properties
        pump.setProperty("EPICSModule", ['vac_ctrl_digitelqpce'])
        pump.setProperty("EPICSSnippet", ['vac_pump_digitelqpce_vpi'])
        pump.setProperty("VacController", digitel.name())
        pump.setProperty("QPCeChannel", channel)
        kwargs["CHANNEL"] = channel
        kwargs["CONTROLLERNAME"] = digitel.name()
        _add_macros(pump, kwargs)

        return pump


    def add_tcp350(self, ioc, name, moxahost, moxaport, **kwargs):
        #
        # Adding Pfeiffer turbo pump controller
        #
        tcp = ioc.addDevice(VacuumFactory.DEVTYPE_CONTROLLER_VPT, name)
        # Properties
        tcp.setProperty("MOXAHostname", moxahost)
        tcp.setProperty("EPICSModule", ['vac_ctrl_tcp350'])
        if self.TCP_MODULE_VERSION:
            tcp.setProperty("EPICSModuleVersion", [self.TCP_MODULE_VERSION])
        tcp.setProperty("EPICSSnippet", ['vac_ctrl_tcp350_moxa'])
        tcp.setProperty("MOXAPort", moxaport)
        kwargs["IPADDR"] = moxahost
        kwargs["PORT"] = moxaport
        _add_macros(tcp, kwargs)

        return tcp


    def add_tcp_pump(self, tcp, name, **kwargs):
        #
        # Adding Pfeiffer turbo pump
        #
        pump = tcp.addDevice(VacuumFactory.DEVTYPE_PUMP_VPT, name)
        # Properties
        pump.setProperty("EPICSModule", ['vac_ctrl_tcp350'])
        pump.setProperty("EPICSSnippet", ['vac_pump_tcp350_vpt'])
        kwargs["CONTROLLERNAME"] = tcp.name()
        _add_macros(pump, kwargs)

        return pump


    def add_pumping_group(self, plc, name):
        #
        # Adding Pumping group
        #
        group = plc.addDevice(VacuumFactory.DEVTYPE_PUMPING_GROUP, name)
        # Properties
#       group.setProperty("EPICSSnippet", ['vac_starting_switchyard'])
#       group.setProperty("EPICSModule", ['vac_starting_switchyard'])

        return group


    def add_primary_pump(self, plc, name):
        #
        # Adding primary pump of type DEVTYPE_PUMP_VPP
        #
        pump = plc.addDevice(VacuumFactory.DEVTYPE_PUMP_VPP, name)

        return pump


    def add_turbo_pump(self, plc, name):
        #
        # Adding turbo pump of type DEVTYPE_PUMP_VPT
        #
        pump = plc.addDevice(VacuumFactory.DEVTYPE_PUMP_VPT, name)

        return pump


    def add_ion_pump_vpi(self, plc, name):
        #
        # Adding ionpump of type DEVTYPE_PUMP_VPI
        #
        pump = plc.addDevice(VacuumFactory.DEVTYPE_PUMP_VPI, name)

        return pump


    def add_ion_pump_vpn(self, plc, name):
        #
        # Adding ionpump of type DEVTYPE_PUMP_VPN
        #
        pump = plc.addDevice(VacuumFactory.DEVTYPE_PUMP_VPN, name)

        return pump


    def add_vvf(self, plc, name, Prev_1 = "", Prev_2 = "", Next_1 = "", Next_2 = ""):
        #
        # Adding vacuum fast gate valve
        #
        vvf = plc.addDevice(VacuumFactory.DEVTYPE_VALVE_VVF, name)
        # Properties
        vvf.setProperty("VVF_ITLck_Prev_1", Prev_1)
        vvf.setProperty("VVF_ITLck_Prev_2", Prev_2)
        vvf.setProperty("VVF_ITLck_Next_1", Next_1)
        vvf.setProperty("VVF_ITLck_Next_2", Next_2)

        return vvf


    def add_vvs(self, plc, name, Prev_1 = "", Prev_2 = "", Next_1 = "", Next_2 = ""):
        #
        # Adding vacuum sector gate valve
        #
        vvs = plc.addDevice(VacuumFactory.DEVTYPE_VALVE_VVS, name)
        # Properties
        vvs.setProperty("VVS_ITLck_Prev_1", Prev_1)
        vvs.setProperty("VVS_ITLck_Prev_2", Prev_2)
        vvs.setProperty("VVS_ITLck_Next_1", Next_1)
        vvs.setProperty("VVS_ITLck_Next_2", Next_2)

        return vvs


    def add_vva(self, plc, name, HW_1 = "", HW_2 = "", HW_3 = "", Prs_1 = "", Prs_2 = "", Open = ""):
        #
        # Adding VVA
        #
        vva = plc.addDevice(VacuumFactory.DEVTYPE_VALVE_VVA, name)
        # Properties
        vva.setProperty("VVAG_ITLck_HW_1", HW_1)
        vva.setProperty("VVAG_ITLck_HW_2", HW_2)
        vva.setProperty("VVAG_ITLck_HW_3", HW_3)
        vva.setProperty("VVAG_ITLck_Prs_1", Prs_1)
        vva.setProperty("VVAG_ITLck_Prs_2", Prs_2)
        vva.setProperty("VVAG_ITLck_Open", Open)

        return vva


    def add_vva_bypass(self, plc, name, HW_1 = "", HW_2 = "", HW_3 = "", Prs_1 = "", Prs_2 = "", Open = ""):
        #
        # Adding VVA-BYPASS
        #
        vva = plc.addDevice(VacuumFactory.DEVTYPE_VALVE_VVA_BYPASS, name)
        # Properties
        vva.setProperty("VVAG_ITLck_HW_1", HW_1)
        vva.setProperty("VVAG_ITLck_HW_2", HW_2)
        vva.setProperty("VVAG_ITLck_HW_3", HW_3)
        vva.setProperty("VVAG_ITLck_Prs_1", Prs_1)
        vva.setProperty("VVAG_ITLck_Prs_2", Prs_2)
        vva.setProperty("VVAG_ITLck_Open", Open)

        return vva


    def add_plcio(self, plc, name, hostname = None):
        #
        # Adding PLCIO device
        #
        plcio = plc.addDevice(VacuumFactory.DEVTYPE_PLCIO, name)
        # Properties
#        plcio.setProperty("Hostname", hostname, str)

        return plcio


    def add_vpsu(self, plc, name):
        #
        # Adding VPSU device
        #
        vpsu = plc.addDevice(VacuumFactory.DEVTYPE_VPSU, name)

        return vpsu


def devname_to_filename(name):
    return name.lower().replace('-', '_').replace(':', '_')


def togit(*args):
    args = list(args)
    if not args[-1].endswith(".git"):
        args[-1] = ".".join([args[-1], "git"])

    return helpers.urljoin(*args)


def _add_macros(dev, kwargs):
    ks = sorted(kwargs.keys())
    if "EPICSModuleVersion" in ks:
        k = "EPICSModuleVersion"
        dev.setProperty(k, [kwargs[k]])
        ks.remove(k)
    for k in ks:
        dev.setProperty("EPICSMacro#{}".format(k), kwargs[k])


#
# DEVICES
#
def add_switchyard(factory, ioc, name, plc, gauge_1, gauge_2, **kwargs):
    #
    # Adding Starting Switchyard of type VACUUM_VAC-VPG
    #
    yard = ioc.addDevice(VacuumFactory.DEVTYPE_PUMPING_GROUP, name)
    # Properties
    yard.setProperty("EPICSSnippet", ['vac_starting_switchyard'])
    yard.setProperty("EPICSModule", ['vac_starting_switchyard'])
    kwargs["PLC"] = plc if isinstance(plc, str) else plc.name()
    kwargs["GAUGE_1"] = gauge_1 if isinstance(gauge_1, str) else gauge_1.name()
    kwargs["GAUGE_2"] = gauge_2 if isinstance(gauge_2, str) else gauge_2.name()
    _add_macros(yard, kwargs)

    return yard


def add_vera(factory, ioc, name, moxahost, moxaport, **kwargs):
    #
    # Adding Residual gas analyzer controller of type VACUUM_VAC-VERA
    #
    vera = ioc.addDevice("VACUUM_VAC-VERA", name)
    # Properties
    vera.setProperty("EPICSModule", [], "Strings List")
    vera.setProperty("EPICSSnippet", [], "Strings List")
    vera.setProperty("MOXAHostname", moxahost)
    vera.setProperty("MOXAPort", moxaport)
    _add_macros(vera, kwargs)

    return vera


def add_rga(factory, vera, name, moxahost, moxaport, **kwargs):
    #
    # Adding residual gas analyzer of type VACUUM_VAC-VGR
    #
    rga = vera.addDevice("VACUUM_VAC-VGR", name)
    # Properties
    rga.setProperty("EPICSModule", ['vac_ctrl_halrcrga'])
    rga.setProperty("VacController", vera.name())
    rga.setProperty("EPICSSnippet", ['vac_ctrl_halrcrga_moxa'])
    rga.setProperty("MOXAHostname", moxahost)
    rga.setProperty("MOXAPort", moxaport)
    kwargs["CONTROLLERNAME"] = vera.name()
    _add_macros(rga, kwargs)

    return rga


def add_coupler(factory, ioc, name, vac_section, ecatio_slave_id, ecatio_hw_type, plc, **kwargs):
    #
    # Adding EtherCAT coupler
    #
    coupler = ioc.addDevice("VACUUM_VAC-ECATIO-COUPLER", name)
    # Properties
    coupler.setProperty("PLCF#VAC_SECTION", vac_section)
    coupler.setProperty("PLCF#ECATIO_COUPLER", name)
    coupler.setProperty("PLCF#ECATIO_SLAVE_ID", ecatio_slave_id, int)
    coupler.setProperty("PLCF#ECATIO_HW_TYPE", ecatio_hw_type, str)
    coupler.setProperty("PLCF#ECATIO_PLC_IDX", plc.idx(), int)
    _add_macros(coupler, kwargs)

    return coupler


def add_daq(factory, coupler, name, ecatio_master_id, ecatio_slave_id, ecatio_hw_type, ecatio_channel, ds, **kwargs):
    #
    # Adding fast DAQ gauge
    #
    daq = coupler.addDevice("VACUUM_VAC-VG-DAQ", name)
    # Properties
    daq.setProperty("PLCF#ECATIO_MASTER_ID", ecatio_master_id, int)
    daq.setProperty("PLCF#ECATIO_SLAVE_ID", ecatio_slave_id, int)
    daq.setProperty("PLCF#ECATIO_HW_TYPE", ecatio_hw_type, str)
    daq.setProperty("PLCF#ECATIO_CHANNEL_NUMBER", ecatio_channel, int)
    daq.setProperty("PLCF#ECATIO_DS_IDX", ds.idx(), int)
    _add_macros(daq, kwargs)

    return daq


def add_tc(factory, coupler, name, ecatio_master_id, ecatio_slave_id, ecatio_hw_type, ecatio_channel, **kwargs):
    #
    # Adding TC thermocouple
    #
    daq = coupler.addDevice("VACUUM_VAC-TC", name)
    # Properties
    daq.setProperty("PLCF#ECATIO_MASTER_ID", ecatio_master_id, int)
    daq.setProperty("PLCF#ECATIO_SLAVE_ID", ecatio_slave_id, int)
    daq.setProperty("PLCF#ECATIO_HW_TYPE", ecatio_hw_type, str)
    daq.setProperty("PLCF#ECATIO_CHANNEL_NUMBER", ecatio_channel, int)
    _add_macros(daq, kwargs)

    return daq


def add_mks(factory, ioc, name, moxahost, moxaport, **kwargs):
    #
    # Adding MKS gauge controller of type DEVTYPE_CONTROLLER_GAUGE
    #
    mks = ioc.addDevice(VacuumFactory.DEVTYPE_CONTROLLER_GAUGE, name)
    # Properties
    mks.setProperty("MOXAHostname", moxahost)
    mks.setProperty("EPICSModule", ['vac_ctrl_mks946_937b'])
    if VacuumFactory.MKS_MODULE_VERSION:
        mks.setProperty("EPICSModuleVersion", [VacuumFactory.MKS_MODULE_VERSION])
    mks.setProperty("EPICSSnippet", ['vac_ctrl_mks946_937b_moxa'])
    mks.setProperty("MOXAPort", moxaport)
    _add_macros(mks, kwargs)

    return mks


def add_mks_vgp(factory, mks, name, channel, **kwargs):
    #
    # Adding MKS Pirani gauge of type DEVTYPE_GAUGE_VGP
    #
    mks_vgp = mks.addDevice(VacuumFactory.DEVTYPE_GAUGE_VGP, name)
    # Properties
    mks_vgp.setProperty("EPICSModule", ['vac_ctrl_mks946_937b'])
    mks_vgp.setProperty("EPICSSnippet", ['vac_gauge_mks_vgp'])
    mks_vgp.setProperty("VacController", mks.name())
    mks_vgp.setProperty("MKSChannel", channel)
    kwargs["CHANNEL"] = channel
    kwargs["CONTROLLERNAME"] = mks.name()
    _add_macros(mks_vgp, kwargs)

    return mks_vgp


def add_mks_vgc(factory, mks, name, channel, **kwargs):
    #
    # Adding MKS Cold cathode gauge of type DEVTYPE_GAUGE_VGC
    #
    mks_vgc = mks.addDevice(VacuumFactory.DEVTYPE_GAUGE_VGC, name)
    # Properties
    mks_vgc.setProperty("EPICSModule", ['vac_ctrl_mks946_937b'])
    mks_vgc.setProperty("EPICSSnippet", ['vac_gauge_mks_vgc'])
    mks_vgc.setProperty("VacController", mks.name())
    mks_vgc.setProperty("MKSChannel", channel)
    kwargs["CHANNEL"] = channel
    kwargs["CONTROLLERNAME"] = mks.name()
    _add_macros(mks_vgc, kwargs)

    return mks_vgc


def add_mks_vgd(factory, mks, name, channel, **kwargs):
    #
    # Adding MKS capacitance manomenter gauge of type DEVTYPE_GAUGE_VGD
    #
    mks_vgd = mks.addDevice(VacuumFactory.DEVTYPE_GAUGE_VGD, name)
    # Properties
    mks_vgd.setProperty("EPICSModule", ['vac_ctrl_mks946_937b'])
    mks_vgd.setProperty("EPICSSnippet", ['vac_gauge_mks_vgd'])
    mks_vgd.setProperty("VacController", mks.name())
    mks_vgd.setProperty("MKSChannel", channel)
    kwargs["CHANNEL"] = channel
    kwargs["CONTROLLERNAME"] = mks.name()
    _add_macros(mks_vgd, kwargs)

    return mks_vgd


def add_mks_vvmc(factory, mks, name, channel, **kwargs):
    #
    # Adding MKS mass flow meter gauge of type DEVTYPE_GAUGE_VVMC
    #
    mks_vvmc = mks.addDevice(VacuumFactory.DEVTYPE_GAUGE_VVMC, name)
    # Properties
    mks_vvmc.setProperty("EPICSModule", ['vac_ctrl_mks946_937b'])
    mks_vvmc.setProperty("EPICSSnippet", ['vac_mfc_mks_gv50a'])
    mks_vvmc.setProperty("VacController", mks.name())
    mks_vvmc.setProperty("MKSChannel", channel)
    kwargs["CHANNEL"] = channel
    kwargs["CONTROLLERNAME"] = mks.name()
    _add_macros(mks_vvmc, kwargs)

    return mks_vvmc


def add_tpg(factory, ioc, name, moxahost, moxaport, **kwargs):
    #
    # Adding TPG gauge controller of type DEVTYPE_CONTROLLER_GAUGE
    #
    tpg = ioc.addDevice(VacuumFactory.DEVTYPE_CONTROLLER_GAUGE, name)
    # Properties
    tpg.setProperty("MOXAHostname", moxahost)
    tpg.setProperty("EPICSModule", ['vac_ctrl_tpg300_500'])
    if VacuumFactory.TPG_MODULE_VERSION:
        tpg.setProperty("EPICSModuleVersion", [VacuumFactory.TPG_MODULE_VERSION])
    tpg.setProperty("EPICSSnippet", ['vac_ctrl_tpg300_500_moxa'])
    tpg.setProperty("MOXAPort", moxaport)
    _add_macros(tpg, kwargs)

    return tpg


def add_tpg_vgc(factory, tpg, name, channel, ecatio_slot = None, ecatio_channel = None, **kwargs):
    #
    # Adding TPG Cold cathode gauge of type DEVTYPE_GAUGE_VGC
    #
    tpg_vgc = tpg.addDevice(VacuumFactory.DEVTYPE_GAUGE_VGC, name)
    # Properties
    tpg_vgc.setProperty("EPICSModule", ['vac_ctrl_tpg300_500'])
    tpg_vgc.setProperty("EPICSSnippet", ['vac_gauge_tpg300_500_vgc'])
    tpg_vgc.setProperty("VacController", tpg.name())
    tpg_vgc.setProperty("TPGChannel", channel)
    tpg_vgc.setProperty("PLCF#ECATIOSlotNumber", ecatio_slot, int)
    tpg_vgc.setProperty("PLCF#ECATIOChannelNumber", ecatio_channel, int)
    kwargs["CHANNEL"] = channel
    kwargs["CONTROLLERNAME"] = tpg.name()
    _add_macros(tpg_vgc, kwargs)

    return tpg_vgc


def add_leybold(factory, ioc, name, moxahost, moxaport, **kwargs):
    #
    # Adding Leybold TD20 turbo pump controller
    #
    leybold = ioc.addDevice(VacuumFactory.DEVTYPE_CONTROLLER_VPT, name)
    # Properties
    leybold.setProperty("EPICSModule", ['vac_ctrl_leyboldtd20'])
    if VacuumFactory.LEYBOLD_MODULE_VERSION:
        leybold.setProperty("EPICSModuleVersion", [VacuumFactory.LEYBOLD_MODULE_VERSION])
    leybold.setProperty("EPICSSnippet", ['vac_ctrl_leyboldtd20_moxa'])
    leybold.setProperty("MOXAHostname", moxahost)
    leybold.setProperty("MOXAPort", moxaport)
#    kwargs["Hostname"] = moxahost
#    kwargs["PORT"] = moxaport
    _add_macros(leybold, kwargs)

    return leybold


def add_leybold_pump(factory, leybold, name, **kwargs):
    #
    # Adding Leybold TD20 turbo pump device
    #
    pump = leybold.addDevice(VacuumFactory.DEVTYPE_PUMP_VPT, name)
    # Properties
    pump.setProperty("EPICSModule", ['vac_ctrl_leyboldtd20'])
    pump.setProperty("EPICSSnippet", ['vac_pump_leyboldtd20_vpt'])
    kwargs["CONTROLLERNAME"] = leybold.name()
    _add_macros(pump, kwargs)

    return pump


def add_digitel(factory, ioc, name, moxahost, moxaport, **kwargs):
    #
    # Adding DigitelQpce ion pump controller as a VEPI
    #
    digitel = ioc.addDevice(VacuumFactory.DEVTYPE_CONTROLLER_VPI, name)
    # Properties
    digitel.setProperty("MOXAHostname", moxahost)
    digitel.setProperty("EPICSModule", ['vac_ctrl_digitelqpce'])
    if VacuumFactory.DIGITEL_MODULE_VERSION:
        digitel.setProperty("EPICSModuleVersion", [VacuumFactory.DIGITEL_MODULE_VERSION])
    digitel.setProperty("EPICSSnippet", ['vac_ctrl_digitelqpce_moxa'])
    digitel.setProperty("MOXAPort", moxaport)
    _add_macros(digitel, kwargs)

    return digitel


def add_digitel_pump_vpi(factory, digitel, name, channel, **kwargs):
    #
    # Adding DigitelQpce ion pump as a VPI
    #
    pump = digitel.addDevice(VacuumFactory.DEVTYPE_PUMP_VPI, name)
    # Properties
    pump.setProperty("EPICSModule", ['vac_ctrl_digitelqpce'])
    pump.setProperty("EPICSSnippet", ['vac_pump_digitelqpce_vpi'])
    pump.setProperty("VacController", digitel.name())
    pump.setProperty("QPCeChannel", channel)
    kwargs["CHANNEL"] = channel
    kwargs["CONTROLLERNAME"] = digitel.name()
    _add_macros(pump, kwargs)

    return pump


def add_digitel_pump_vpn(factory, digitel, name, channel, **kwargs):
    #
    # Adding DigitelQpce ion pump as a VPN
    #
    pump = digitel.addDevice(VacuumFactory.DEVTYPE_PUMP_VPN, name)
    # Properties
    pump.setProperty("EPICSModule", ['vac_ctrl_digitelqpce'])
    pump.setProperty("EPICSSnippet", ['vac_pump_digitelqpce_vpi'])
    pump.setProperty("VacController", digitel.name())
    pump.setProperty("QPCeChannel", channel)
    kwargs["CHANNEL"] = channel
    kwargs["CONTROLLERNAME"] = digitel.name()
    _add_macros(pump, kwargs)

    return pump


def add_tcp350(factory, ioc, name, moxahost, moxaport, **kwargs):
    #
    # Adding Pfeiffer turbo pump controller
    #
    tcp = ioc.addDevice(VacuumFactory.DEVTYPE_CONTROLLER_VPT, name)
    # Properties
    tcp.setProperty("MOXAHostname", moxahost)
    tcp.setProperty("EPICSModule", ['vac_ctrl_tcp350'])
    if VacuumFactory.TCP_MODULE_VERSION:
        tcp.setProperty("EPICSModuleVersion", [VacuumFactory.TCP_MODULE_VERSION])
    tcp.setProperty("EPICSSnippet", ['vac_ctrl_tcp350_moxa'])
    tcp.setProperty("MOXAPort", moxaport)
    _add_macros(tcp, kwargs)

    return tcp


def add_tcp_pump(factory, tcp, name, **kwargs):
    #
    # Adding Pfeiffer turbo pump
    #
    pump = tcp.addDevice(VacuumFactory.DEVTYPE_PUMP_VPT, name)
    # Properties
    pump.setProperty("EPICSModule", ['vac_ctrl_tcp350'])
    pump.setProperty("EPICSSnippet", ['vac_pump_tcp350_vpt'])
    kwargs["CONTROLLERNAME"] = tcp.name()
    _add_macros(pump, kwargs)

    return pump


def add_pumping_group(factory, plc, name):
    #
    # Adding Pumping group
    #
    group = plc.addDevice(VacuumFactory.DEVTYPE_PUMPING_GROUP, name)
    # Properties
#    group.setProperty("EPICSSnippet", ['vac_starting_switchyard'])
#    group.setProperty("EPICSModule", ['vac_starting_switchyard'])

    return group


def add_primary_pump(factory, plc, name):
    #
    # Adding primary pump of type DEVTYPE_PUMP_VPP
    #
    pump = plc.addDevice(VacuumFactory.DEVTYPE_PUMP_VPP, name)

    return pump


def add_turbo_pump(factory, plc, name):
    #
    # Adding turbo pump of type DEVTYPE_PUMP_VPT
    #
    pump = plc.addDevice(VacuumFactory.DEVTYPE_PUMP_VPT, name)

    return pump


DEFAULT_PREV_1 = "Prev-1"
DEFAULT_PREV_2 = "Prev-2"
DEFAULT_NEXT_1 = "Next-1"
DEFAULT_NEXT_2 = "Next-2"
def add_vvs(factory, plc, name, PREV_1 = DEFAULT_PREV_1, PREV_2 = DEFAULT_PREV_2, NEXT_1 = DEFAULT_NEXT_1, NEXT_2 = DEFAULT_NEXT_2):
    #
    # Adding vacuum sector gate valve
    #
    vvs = plc.addDevice(VacuumFactory.DEVTYPE_VALVE_VVS, name)
    # Properties
    def add_colon(default, label):
        if not default == label:
            label = "{} - {}".format(default, label)
        if not label.endswith(":"):
            return "{}:".format(label)
        return label

    vvs.setProperty("VVS_ITLck_Prev_1", add_colon(DEFAULT_PREV_1, PREV_1))
    vvs.setProperty("VVS_ITLck_Prev_2", add_colon(DEFAULT_PREV_2, PREV_2))
    vvs.setProperty("VVS_ITLck_Next_1", add_colon(DEFAULT_NEXT_1, NEXT_1))
    vvs.setProperty("VVS_ITLck_Next_2", add_colon(DEFAULT_NEXT_2, NEXT_2))

    return vvs


def add_vva(factory, plc, name, HW_1 = "", HW_2 = "", HW_3 = "", Prs_1 = "", Prs_2 = "", Open = ""):
    #
    # Adding VVA
    #
    vva = plc.addDevice(VacuumFactory.DEVTYPE_VALVE_VVA, name)
    # Properties
    vva.setProperty("VVAG_ITLck_HW_1", HW_1)
    vva.setProperty("VVAG_ITLck_HW_2", HW_2)
    vva.setProperty("VVAG_ITLck_HW_3", HW_3)
    vva.setProperty("VVAG_ITLck_Prs_1", Prs_1)
    vva.setProperty("VVAG_ITLck_Prs_2", Prs_2)
    vva.setProperty("VVAG_ITLck_Open", Open)

    return vva


def add_vva_bypass(factory, plc, name, HW_1 = "", HW_2 = "", HW_3 = "", Prs_1 = "", Prs_2 = "", Open = ""):
    #
    # Adding VVA-BYPASS
    #
    vva = plc.addDevice(VacuumFactory.DEVTYPE_VALVE_VVA_BYPASS, name)
    # Properties
    vva.setProperty("VVAG_ITLck_HW_1", HW_1)
    vva.setProperty("VVAG_ITLck_HW_2", HW_2)
    vva.setProperty("VVAG_ITLck_HW_3", HW_3)
    vva.setProperty("VVAG_ITLck_Prs_1", Prs_1)
    vva.setProperty("VVAG_ITLck_Prs_2", Prs_2)
    vva.setProperty("VVAG_ITLck_Open", Open)

    return vva


add_vvg = add_vva


def add_plcio(factory, plc, name, hostname = None):
    #
    # Adding PLCIO device
    #
    plcio = plc.addDevice(VacuumFactory.DEVTYPE_PLCIO, name)
    # Properties
    plcio.setProperty("Hostname", hostname)

    return plcio


def add_vpsu(factory, plc, name):
    #
    # Adding VPSU device
    #
    vpsu = plc.addDevice(VacuumFactory.DEVTYPE_VPSU, name)

    return vpsu
