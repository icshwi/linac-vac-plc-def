#!/usr/bin/env python

import os
import sys

sys.path.append(os.path.curdir)
thisdir = os.path.abspath(os.path.dirname(__file__))
sys.path.append(thisdir)

from ccdb_factory import CCDB_Factory
from vac_factory_helpers import *


epi_git = "https://gitlab.esss.lu.se/icshwi/linac-vac-plc-def"
epi_wc = os.path.dirname(thisdir)
ioc_git = "https://gitlab.esss.lu.se/ioc/"
HOST = "vac-ipc-daq001-lebt"
EPICS_VERSION = "7.0.5"
E3_REQUIRE_VERSION = "3.4.1"


def initialize_factory(factory = None):
    if factory is None:
        factory = CCDB_Factory()

    # Artifacts for VACUUM_VAC-VG-DAQ
    factory.addArtifact("VACUUM_VAC-VG-DAQ", "TEMPLATE_VAC-DAQ-DB.txt")
    factory.addArtifact("VACUUM_VAC-VG-DAQ", "TEMPLATE_ARCHIVE.txt", "TEMPLATE_VAC-DAQ-GAUGE-ARCHIVE.txt")

    # Artifacts for VACUUM_VAC-TC
    factory.addArtifact("VACUUM_VAC-TC", "TEMPLATE_VAC-TC-DB.txt")
    factory.addArtifact("VACUUM_VAC-TC", "TEMPLATE_ARCHIVE.txt", "TEMPLATE_VAC-TC-ARCHIVE.txt")

    # Artifacts for VACUUM_VAC-ECATIO-COUPLER
    factory.addArtifact("VACUUM_VAC-ECATIO-COUPLER", "HEADER_TEMPLATE_VAC-DAQ-DS-PLC.txt")
    factory.addArtifact("VACUUM_VAC-ECATIO-COUPLER", "TEMPLATE_VAC-DAQ-DS-PLC-IOCSH.txt")

    return factory


class ECATIO_IDX(object):
    def __init__(self):
        self._idx = 0


    def idx(self):
        self._idx += 1

        return self._idx - 1


#                             DAQ
DAQ_IOC = "VacS-ACCV:Ctrl-IOC-327"
def create_daq_ioc(factory, name = DAQ_IOC, host = HOST):
    master_id = 0
    coupler_hw_type = "EK1101"
    gauge_hw_type = "EL3164"
    tc_hw_type = "EL3314"
    ds_template = "TEMPLATE_VAC-DAQ-DS.txt"
    ds_plc_template = "TEMPLATE_VAC-DAQ-DS-PLC.txt"
    ds = ECATIO_IDX()
    plc = ECATIO_IDX()

    if isinstance(name, str):
        #
        # Adding IOC of type IOC
        #
        ioc = factory.addDevice("IOC", name, description="IOC for Fast Data Acquisition of vacuum gauges")
        # Properties
        ioc.setProperty("Hostname", host)
        ioc.addLink("IOC_REPOSITORY", "{}/{}.git".format(ioc_git, devname_to_filename(name)), download = False)
        ioc.setProperty("EPICSVersion", EPICS_VERSION)
        ioc.setProperty("E3RequireVersion", E3_REQUIRE_VERSION)
    else:
        ioc = name

    ioc.addArtifact("HEADER_TEMPLATE_VAC-DAQ-DB.txt")
    ioc.addArtifact("FOOTER_TEMPLATE_VAC-DAQ-DB.txt")

    ioc.addArtifact("HEADER_TEMPLATE_VAC-TC-DB.txt")
    ioc.addArtifact("FOOTER_TEMPLATE_VAC-TC-DB.txt")

    ioc.addArtifact("HEADER_TEMPLATE_VAC-DAQ-DS.txt")
    ioc.addArtifact("FOOTER_TEMPLATE_VAC-DAQ-DS.txt")

    ioc.addArtifact("HEADER_TEMPLATE_VAC-DAQ-DS-PLC-IOCSH.txt")


    #
    # Adding LEBT DAQ coupler
    #
    slave_id = 0
    coupler = add_coupler(factory, ioc, "LEBT-010:Vac-ECATIO-10001", "lebt", slave_id, coupler_hw_type, plc)

    #
    # Adding LEBT DAQ gauges
    #
    slave_id = 1
    add_daq(factory, coupler, "LEBT-010:Vac-VGC-10000", master_id, slave_id, gauge_hw_type, 1, ds).addArtifact(ds_template).addArtifact(ds_plc_template)
    add_daq(factory, coupler, "LEBT-010:Vac-VGC-30000", master_id, slave_id, gauge_hw_type, 3, ds).addArtifact(ds_template).addArtifact(ds_plc_template)
    add_daq(factory, coupler, "LEBT-010:Vac-VGD-10000", master_id, slave_id, gauge_hw_type, 2, ds).addArtifact(ds_template).addArtifact(ds_plc_template)
#   add_dag(factory, coupler, "LEBT-010:Vac-VPM-01100", master_id, slave_id, gauge_hw_type, 4) # or LEBT-010:Vac-VPM-00011 :shrug:

    slave_id = 2
#   add_daq(factory, coupler, "LEBT-010:Vac-VGC-99999", master_id, slave_id, gauge_hw_type, 1)

    #
    # Adding RFQ DAQ coupler
    #
    slave_id = 3
    coupler = add_coupler(factory, ioc, "RFQ-010:Vac-ECATIO-10002", "rfq", slave_id, coupler_hw_type, plc)

    #
    # Adding RFQ DAQ gagues
    #
    slave_id = 4
    add_daq(factory, coupler, "RFQ-010:Vac-VGC-10000", master_id, slave_id, gauge_hw_type, 1, ds).addArtifact(ds_template).addArtifact(ds_plc_template)
    add_daq(factory, coupler, "RFQ-010:Vac-VGC-30000", master_id, slave_id, gauge_hw_type, 2, ds).addArtifact(ds_template).addArtifact(ds_plc_template)
    add_daq(factory, coupler, "RFQ-010:Vac-VGC-20000", master_id, slave_id, gauge_hw_type, 3, ds).addArtifact(ds_template).addArtifact(ds_plc_template)
    add_daq(factory, coupler, "RFQ-010:Vac-VGC-40000", master_id, slave_id, gauge_hw_type, 4, ds).addArtifact(ds_template).addArtifact(ds_plc_template)

    # empty EL3164
    slave_id = 5

    #
    # Adding RFQ TC thermocouples
    #
    slave_id = 6
    add_tc(factory, coupler, "RFQ-010:Vac-VTC-00011", master_id, slave_id, tc_hw_type, 1)
    add_tc(factory, coupler, "RFQ-010:Vac-VTC-00012", master_id, slave_id, tc_hw_type, 3)

    #
    # Adding MEBT DAQ coupler
    #
    slave_id = 7
    coupler = add_coupler(factory, ioc, "MEBT-010:Vac-ECATIO-10003", "mebt", slave_id, coupler_hw_type, plc)

    #
    # Adding MEBT DAQ gagues
    #
    slave_id = 8
    add_daq(factory, coupler, "MEBT-010:Vac-VGC-10000", master_id, slave_id, gauge_hw_type, 1, ds).addArtifact(ds_template).addArtifact(ds_plc_template)
    add_daq(factory, coupler, "MEBT-010:Vac-VGC-30000", master_id, slave_id, gauge_hw_type, 2, ds).addArtifact(ds_template).addArtifact(ds_plc_template)
    add_daq(factory, coupler, "MEBT-010:Vac-VGC-20000", master_id, slave_id, gauge_hw_type, 3, ds).addArtifact(ds_template).addArtifact(ds_plc_template)

    #
    # Adding DTL DAQ coupler
    #
    slave_id = 9
    coupler = add_coupler(factory, ioc, "DTL-010:Vac-ECATIO-10004", "dtl", slave_id, coupler_hw_type, plc)

    #
    # Adding DTL DAQ gagues
    #
    slave_id = 10
    add_daq(factory, coupler, "DTL-010:Vac-VGC-10000", master_id, slave_id, gauge_hw_type, 1, ds)
    add_daq(factory, coupler, "DTL-020:Vac-VGC-10000", master_id, slave_id, gauge_hw_type, 2, ds)
    add_daq(factory, coupler, "DTL-010:Vac-VGC-50000", master_id, slave_id, gauge_hw_type, 3, ds)
    add_daq(factory, coupler, "DTL-020:Vac-VGC-50000", master_id, slave_id, gauge_hw_type, 4, ds)

    slave_id = 11
    add_daq(factory, coupler, "DTL-030:Vac-VGC-10000", master_id, slave_id, gauge_hw_type, 1, ds)
    add_daq(factory, coupler, "DTL-040:Vac-VGC-10000", master_id, slave_id, gauge_hw_type, 2, ds)
    add_daq(factory, coupler, "DTL-030:Vac-VGC-50000", master_id, slave_id, gauge_hw_type, 3, ds)
    add_daq(factory, coupler, "DTL-040:Vac-VGC-50000", master_id, slave_id, gauge_hw_type, 4, ds)

    slave_id = 12
    add_daq(factory, coupler, "DTL-050:Vac-VGC-10000", master_id, slave_id, gauge_hw_type, 1, ds)
    add_daq(factory, coupler, "DTL-020:Vac-VGC-20000", master_id, slave_id, gauge_hw_type, 2, ds)
    add_daq(factory, coupler, "DTL-050:Vac-VGC-50000", master_id, slave_id, gauge_hw_type, 3, ds)
    add_daq(factory, coupler, "DTL-030:Vac-VGC-20000", master_id, slave_id, gauge_hw_type, 4, ds)

    slave_id = 13
    add_daq(factory, coupler, "DTL-040:Vac-VGC-20000", master_id, slave_id, gauge_hw_type, 1, ds)
    add_daq(factory, coupler, "DTL-050:Vac-VGC-20000", master_id, slave_id, gauge_hw_type, 3, ds)

    #
    # Adding DTL TC thermocouples
    #
    slave_id = 14
    add_tc(factory, coupler, "DTL-010:Vac-VTC-00011", master_id, slave_id, tc_hw_type, 1)
    add_tc(factory, coupler, "DTL-010:Vac-VTC-00012", master_id, slave_id, tc_hw_type, 2)
    add_tc(factory, coupler, "DTL-020:Vac-VTC-00011", master_id, slave_id, tc_hw_type, 3)
    add_tc(factory, coupler, "DTL-020:Vac-VTC-00012", master_id, slave_id, tc_hw_type, 4)

    slave_id = 15
    add_tc(factory, coupler, "DTL-030:Vac-VTC-00011", master_id, slave_id, tc_hw_type, 1)
    add_tc(factory, coupler, "DTL-030:Vac-VTC-00012", master_id, slave_id, tc_hw_type, 2)
    add_tc(factory, coupler, "DTL-040:Vac-VTC-00011", master_id, slave_id, tc_hw_type, 3)
    add_tc(factory, coupler, "DTL-040:Vac-VTC-00012", master_id, slave_id, tc_hw_type, 4)

    slave_id = 16
    add_tc(factory, coupler, "DTL-050:Vac-VTC-00011", master_id, slave_id, tc_hw_type, 1)
    add_tc(factory, coupler, "DTL-050:Vac-VTC-00012", master_id, slave_id, tc_hw_type, 2)

    return ioc


create_ioc = create_daq_ioc



if __name__ == "__main__":
    #
    # Create factory
    #
    factory = initialize_factory()

    #
    # Create DAQ IOC
    #
    create_daq_ioc(factory)

    #
    # Saving the created CCDB
    #
    factory.save(DAQ_IOC)
