#FILENAME ecat_ds_setup-[PLCF#TIMESTAMP].iocsh
epicsEnvSet("DS_SIZE", "${VAC_DAQ_BUF_SIZE}")
#-  DS_TYPE = 0 : Fill from beginning. Stop when full
#-  DS_TYPE = 1 : Fill from beginning. Start over in beginning
#-  DS_TYPE = 2 : Fill from end (newwst value in the end). Old values shifted out
epicsEnvSet("DS_TYPE", 2)  # Fill from beginning. Start over in beginning
epicsEnvSet("DS_SAMPLE_RATE_MS", -1) # (push data when triggered)

