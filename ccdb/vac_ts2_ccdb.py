#!/usr/bin/env python

import os
import sys

thisdir = os.path.abspath(os.path.dirname(__file__))
sys.path.append(thisdir)

import vac_factory_helpers


epi_wc = os.path.dirname(thisdir)
epi_wc = None
HOST = "vacs-ts2-vm-ioc"
EPICS_VERSION = vac_factory_helpers.VacuumFactory.EPICS_VERSION
E3_REQUIRE_VERSION = vac_factory_helpers.VacuumFactory.E3_REQUIRE_VERSION


def initialize_factory(factory = None):
    if factory is None:
        factory = vac_factory_helpers.VacuumFactory(epi_wc)

    factory.add_gauge_devtypes()

    factory.ts2_moxa = "moxa-vac-ts2.tn.esss.lu.se"

    return factory


MKS_GAUGE_IOC = "VacS-TS2CRM:SC-IOC-111"
def create_ts2_mks_ioc(factory, name = MKS_GAUGE_IOC, host = HOST):
    ts2_moxa = factory.ts2_moxa

    #
    # Adding IOC
    #
    ioc = factory.add_ioc(name, host, description="IOC of MKS gauge controllers")

    #
    # Adding device TS2-020Row:Vac-VEG-01100 of type VACUUM_VAC-VEG|VAC-VEVMC
    #
    mks = factory.add_mks(ioc, "TS2-020Row:Vac-VEG-01100", ts2_moxa, 4001, BOARD_A_SERIAL_NUMBER = "1902131114", BOARD_B_SERIAL_NUMBER = "1812101207", BOARD_C_SERIAL_NUMBER = "1812100755")
    factory.add_mks_vgp(mks, "TS2-010CRM:Vac-VGP-01100", "A1")
    factory.add_mks_vgc(mks, "TS2-010CRM:Vac-VGC-01100", "B1")

    return ioc


TPG_GAUGE_IOC = "VacS-TS2CRM:SC-IOC-112"
def create_ts2_tpg_ioc(factory, name = TPG_GAUGE_IOC, host = HOST):
    ts2_moxa = factory.ts2_moxa

    #
    # Adding IOC
    #
    ioc = factory.add_ioc(name, host, description="IOC to control TPG gauge controllers")

    #
    # Adding device TS2-020Row:Vac-VEG-10001 of type VACUUM_VAC-VEG|VAC-VEVMC
    #
    tpg = factory.add_tpg(ioc, "TS2-020Row:Vac-VEG-10001", ts2_moxa, 4002)
    factory.add_tpg_vgc(tpg, "TS2-010CRM:Vac-VGC-10000", "A1")
    factory.add_tpg_vgc(tpg, "TS2-010CRM:Vac-VGC-20000", "B1")

    #
    # Adding device TS2-020Row:Vac-VEG-10002 of type VACUUM_VAC-VEG|VAC-VEVMC
    #
    tpg = factory.add_tpg(ioc, "TS2-020Row:Vac-VEG-10002", ts2_moxa, 4003)
    factory.add_tpg_vgc(tpg, "TS2-010CRM:Vac-VGC-30000", "A1")
    factory.add_tpg_vgc(tpg, "TS2-010CRM:Vac-VGC-40000", "B1")

    return ioc


SENS4_GAUGE_IOC = "VacS-TS2CRM:SC-IOC-113"
def create_ts2_sens4_ioc(factory, name = TPG_GAUGE_IOC, host = HOST):
    ts2_moxa = factory.ts2_moxa

    #
    # Adding IOC
    #
    ioc = factory.add_ioc(name, host, description="IOC to control SmartPirani gauges")

    #
    # Adding device TS2-020Row:Vac-VEG-10001 of type VACUUM_VAC-VEG|VAC-VEVMC
    #
#    tpg = factory.add_tpg(ioc, "TS2-020Row:Vac-VEG-10001", ts2_moxa, 4002)
#    factory.add_tpg_vgc(tpg, "TS2-010CRM:Vac-VGC-10000", "A1")
#    factory.add_tpg_vgc(tpg, "TS2-010CRM:Vac-VGC-20000", "B1")

    return ioc


TS2CRM_IOC = "VacS-TS2CRM:SC-IOC-100"
def create_ts2_ioc(factory, name = TS2CRM_IOC, host = HOST):
    #
    # Adding IOC
    #
    ioc = factory.add_ioc(name, host)

    create_ts2_mks_ioc(factory, ioc, host)
    create_ts2_tpg_ioc(factory, ioc, host)

    return ioc


create_ioc = create_ts2_ioc



if __name__ == "__main__":
    #
    # Create factory
    #
    factory = initialize_factory()

    #
    # Create MKS Gauge IOC
    #
    create_ts2_mks_ioc(factory)

    #
    # Create TPG Gauge IOC
    #
    create_ts2_tpg_ioc(factory)

    #
    # Saving the created CCDB
    #
    factory.save("VacS-TS2CRM:SC-IOC-all")
