## [PLCF#INSTALLATION_SLOT]
# Rotor frequency (Hz)
[PLCF#INSTALLATION_SLOT]:FreqR
# Motor Current (A)
[PLCF#INSTALLATION_SLOT]:MotorCurR
# Controller Active Failure ID
[PLCF#INSTALLATION_SLOT]:FlrIDR
########################################
