#!/usr/bin/env python

import os
import sys

thisdir = os.path.abspath(os.path.dirname(__file__))
sys.path.append(thisdir)

import vac_factory_helpers


epi_wc = os.path.dirname(thisdir)
epi_wc = None
HOST = "vacs-accv-vm-ioc"
EPICS_VERSION = vac_factory_helpers.VacuumFactory.EPICS_VERSION
E3_REQUIRE_VERSION = vac_factory_helpers.VacuumFactory.E3_REQUIRE_VERSION


def initialize_factory(factory = None):
    if factory is None:
        factory = vac_factory_helpers.VacuumFactory(epi_wc)

    factory.add_plcio_devtypes(plc = True)
    factory.add_vpsu_devtypes(plc = True)
    factory.add_pumping_group_devtypes(plc = True)
    factory.add_primarypump_devtypes(plc = True)
    factory.add_turbopump_devtypes(plc = True)
    factory.add_ionpump_devtypes(plc = True)
    factory.add_valve_devtypes(plc = True)

    return factory


def commissioning_tank(factory, dev1):
# Commissioning tank
    #
    # Adding device LEBT-010:Vac-VVA-99998
    #
    factory.add_vva(dev1, "LEBT-010:Vac-VVA-99998")

    #
    # Adding device LEBT-010:Vac-VPT-99998
    #
    factory.add_turbo_pump(dev1, "LEBT-010:Vac-VPT-99998")

    #
    # Adding device LEBT-010:Vac-VVA-99999
    #
    factory.add_vva(dev1, "LEBT-010:Vac-VVA-99999")

    #
    # Adding device LEBT-010:Vac-VPT-99999
    #
    factory.add_turbo_pump(dev1, "LEBT-010:Vac-VPT-99999")


def create_process_plc_ioc(factory, name = "VacS-ACCV:SC-IOC-001", host = HOST):
    #
    # Adding IOC
    #
    ioc = factory.add_ioc(name, host, description="IOC for Process PLC of High level Proton Beam Vacuum Control System", ioc_repo="https://gitlab.esss.lu.se/iocs/vacs/accv/vacs_accv_sc_ioc_001")

    #
    # Adding PLC: VacS-ACCV:Vac-PLC-01001
    #
    plc = ioc.addPLC("VacS-ACCV:Vac-PLC-01001")
    # Properties
    plc.setProperty("PLCF#PLC-EPICS-COMMS:Endianness", "BigEndian")
    plc.setProperty("PLCF#EPICSToPLCDataBlockStartOffset", "0")
    plc.setProperty("PLCF#PLCToEPICSDataBlockStartOffset", "0")
    plc.setProperty("PLCF#PLC-EPICS-COMMS: MBConnectionID", "255")
    plc.setProperty("PLCF#PLC-EPICS-COMMS: MBPort", "502")
    plc.setProperty("PLCF#PLC-EPICS-COMMS: S7ConnectionID", "256")
    plc.setProperty("PLCF#PLC-EPICS-COMMS: InterfaceID", "72")
    plc.setProperty("PLCF#PLC-DIAG:Max-Modules-In-IO-Device", "20")
    plc.setProperty("EPICSSnippet", [])
    plc.setProperty("PLCF#PLC-DIAG:Max-IO-Devices", "10")
    plc.setProperty("PLCF#PLC-EPICS-COMMS: S7Port", "2000")
    plc.setProperty("PLCF#PLC-DIAG:Max-Local-Modules", "10")
    plc.setProperty("EPICSModule", [])
    plc.setProperty("PLCF#PLC-EPICS-COMMS: PLCPulse", "Pulse_200ms")
    plc.setProperty("PLCF#PLC-EPICS-COMMS: DiagPort", "2001")
    plc.setProperty("PLCF#PLC-EPICS-COMMS: DiagConnectionID", "254")
    plc.setProperty("Hostname", "vac-plc-proc.tn.esss.lu.se")
    # External links
    plc.addLink("EPI[PLC-proc]", factory.git_repo(), epi_wc)
    plc.addLink("BEAST TEMPLATE[PLC-proc]", factory.git_repo(), epi_wc)

    """
        ISrc
    """
    create_isrc_devices(factory, plc)

    """
        LEBT
    """
    create_lebt_devices(factory, plc)

    """
        RFQ
    """
    create_rfq_devices(factory, plc)

    """
        MEBT
    """
    create_mebt_devices(factory, plc)

    """
        DTL
    """
    create_dtl_devices(factory, plc)

    return ioc


def create_isrc_devices(factory, plc):
    #
    # Adding device ISrc-010:Vac-VVA-01100 of type VACUUM_VAC-VVA_ISRC
    #
    dev1 = plc.addDevice("VACUUM_VAC-VVA_ISRC", "ISrc-010:Vac-VVA-01100")
    dev1.addLink("EPI", factory.git_repo(), epi_wc)
    dev1.setProperty("ISRC_PREFIX", "ISrc-LPS::Vac-VVA-01100_")


def create_lebt_devices(factory, plc):
    #
    # Adding device LEBT-010:Vac-PLCIO-01001
    #
    plcio = factory.add_plcio(plc, "LEBT-010:Vac-PLCIO-01001")

    #
    # Adding device LEBT-010:Vac-VPSU-00010
    #
    factory.add_vpsu(plcio, "LEBT-010:Vac-VPSU-00010")

    #
    # Adding device LEBT-010:Vac-VVA-00031
    #
    factory.add_vva(plcio, "LEBT-010:Vac-VVA-00031", HW_1 = "LEBT-010:Vac-VPDP-00031", Prs_2 = "LEBT-010:Vac-VGP-00021")

    #
    # Adding device LEBT-010:Vac-VPG-10001
    #
    group = factory.add_pumping_group(plcio, "LEBT-010:Vac-VPG-10001")

    #
    # Adding device LEBT-010:Vac-VVA-00031
    #
    factory.add_vva(group, "LEBT-010:Vac-VVA-00031", HW_1 = "LEBT-010:Vac-VPDP-00031", Prs_2 = "LEBT-010:Vac-VGP-00021")

    #
    # Adding device LEBT-010:Vac-VVA-02100
    #
    factory.add_vva(group, "LEBT-010:Vac-VVA-02100", HW_2 = "LEBT-010:Vac-VPT-02100", Prs_1 = "LEBT-010:Vac-VGP-00021")

    #
    # Adding device LEBT-010:Vac-VPT-02100
    #
    factory.add_turbo_pump(group, "LEBT-010:Vac-VPT-02100")

    #
    # Adding device LEBT-010:Vac-VVA-03100
    #
    factory.add_vva(group, "LEBT-010:Vac-VVA-03100", HW_2 = "LEBT-010:Vac-VPT-03100", Prs_1 = "LEBT-010:Vac-VGP-00021")

    #
    # Adding device LEBT-010:Vac-VPDP-00031
    #
    factory.add_primary_pump(group, "LEBT-010:Vac-VPDP-00031")

    #
    # Adding device LEBT-010:Vac-VPT-03100
    #
    factory.add_turbo_pump(group, "LEBT-010:Vac-VPT-03100")

    #
    # Adding device LEBT-010:Vac-VVA-00041
    #
    factory.add_vva_bypass(plcio, "LEBT-010:Vac-VVA-00041")

    #
    # Adding device LEBT-010:Vac-VVA-01100
    #
    factory.add_vva(plcio, "LEBT-010:Vac-VVA-01100", Prs_1 = "LEBT-010:Vac-VGC-10000")

    #
    # Adding device LEBT-010:Vac-VPG-20001
    #
    group = factory.add_pumping_group(plc, "LEBT-010:Vac-VPG-20001")

    #
    # Adding device LEBT-010:Vac-VPDP-00071
    #
    factory.add_primary_pump(group, "LEBT-010:Vac-VPDP-00071")

    #
    # Adding device LEBT-010:Vac-VVA-00071
    #
    factory.add_vva(group, "LEBT-010:Vac-VVA-00071", HW_1 = "LEBT-010:Vac-VPDP-00071", Prs_2 = "LEBT-010:Vac-VGP-00081")

    #
    # Adding device LEBT-010:Vac-VVA-06100
    #
    factory.add_vva(group, "LEBT-010:Vac-VVA-06100", HW_2 = "LEBT-010:Vac-VPT-06100", Prs_1 = "LEBT-010:Vac-VGP-00081")

    #
    # Adding device LEBT-010:Vac-VPT-06100
    #
    factory.add_turbo_pump(group, "LEBT-010:Vac-VPT-06100")

    #
    # Adding device LEBT-010:Vac-VVA-07100
    #
    factory.add_vva(group, "LEBT-010:Vac-VVA-07100", HW_2 = "LEBT-010:Vac-VPT-07100", Prs_1 = "LEBT-010:Vac-VGP-00081")

    #
    # Adding device LEBT-010:Vac-VPT-07100
    #
    factory.add_turbo_pump(group, "LEBT-010:Vac-VPT-07100")


def create_rfq_devices(factory, plc):
    #
    # Adding device RFQ-010:Vac-VPSU-00010
    #
    factory.add_vpsu(plc, "RFQ-010:Vac-VPSU-00010")

    #
    # Adding device RFQ-010:Vac-PLCIO-01002
    #
    plcio = factory.add_plcio(plc, "RFQ-010:Vac-PLCIO-01002")

    #
    # Adding device RFQ-010:Vac-VPSU-00020
    #
    factory.add_vpsu(plc, "RFQ-010:Vac-VPSU-00020")

    #
    # Adding device RFQ-010:Vac-PLCIO-01003
    #
    plcio = factory.add_plcio(plc, "RFQ-010:Vac-PLCIO-01003")

    #
    # Adding device RFQ-010:Vac-VPG-10001
    #
    group = factory.add_pumping_group(plc, "RFQ-010:Vac-VPG-10001")

    #
    # Adding device RFQ-010:Vac-VPDP-00031
    #
    factory.add_primary_pump(group, "RFQ-010:Vac-VPDP-00031")

    #
    # Adding device RFQ-010:Vac-VPT-01100
    #
    factory.add_turbo_pump(group, "RFQ-010:Vac-VPT-01100")

    #
    # Adding device RFQ-010:Vac-VPT-02100
    #
    factory.add_turbo_pump(group, "RFQ-010:Vac-VPT-02100")

    #
    # Adding device RFQ-010:Vac-VPT-03100
    #
    factory.add_turbo_pump(group, "RFQ-010:Vac-VPT-03100")

    #
    # Adding device RFQ-010:Vac-VPT-04100
    #
    factory.add_turbo_pump(group, "RFQ-010:Vac-VPT-04100")

    #
    # Adding device RFQ-010:Vac-VPT-05100
    #
    factory.add_turbo_pump(group, "RFQ-010:Vac-VPT-05100")

    #
    # Adding device RFQ-010:Vac-VVG-01100
    #
    factory.add_vvg(group, "RFQ-010:Vac-VVG-01100", HW_2 = "RFQ-010:Vac-VPT-01100", Prs_1 = "RFQ-010:Vac-VGP-05100")

    #
    # Adding device RFQ-010:Vac-VVG-02100
    #
    factory.add_vvg(group, "RFQ-010:Vac-VVG-02100", HW_2 = "RFQ-010:Vac-VPT-02100", Prs_1 = "RFQ-010:Vac-VGP-05100")

    #
    # Adding device RFQ-010:Vac-VVG-03100
    #
    factory.add_vvg(group, "RFQ-010:Vac-VVG-03100", HW_2 = "RFQ-010:Vac-VPT-03100", Prs_1 = "RFQ-010:Vac-VGP-05100")

    #
    # Adding device RFQ-010:Vac-VVG-04100
    #
    factory.add_vvg(group, "RFQ-010:Vac-VVG-04100", HW_2 = "RFQ-010:Vac-VPT-04100", Prs_1 = "RFQ-010:Vac-VGP-05100")

    #
    # Adding device RFQ-010:Vac-VVG-05100
    #
    factory.add_vvg(group, "RFQ-010:Vac-VVG-05100", HW_2 = "RFQ-010:Vac-VPT-05100", Prs_1 = "RFQ-010:Vac-VGP-05100")

    #
    # Adding device RFQ-010:Vac-VVA-00031
    #
    factory.add_vva(group, "RFQ-010:Vac-VVA-00031", HW_1 = "RFQ-010:Vac-VPDP-00031", Prs_2 = "RFQ-010:Vac-VGP-05100")

    #
    # Adding device RFQ-010:Vac-VPG-20001
    #
    group = factory.add_pumping_group(plc, "RFQ-010:Vac-VPG-20001")

    #
    # Adding device RFQ-010:Vac-VPDP-00041
    #
    factory.add_primary_pump(group, "RFQ-010:Vac-VPDP-00041")

    #
    # Adding device RFQ-010:Vac-VPT-06100
    #
    factory.add_turbo_pump(group, "RFQ-010:Vac-VPT-06100")

    #
    # Adding device RFQ-010:Vac-VPT-07100
    #
    factory.add_turbo_pump(group, "RFQ-010:Vac-VPT-07100")

    #
    # Adding device RFQ-010:Vac-VPT-08100
    #
    factory.add_turbo_pump(group, "RFQ-010:Vac-VPT-08100")

    #
    # Adding device RFQ-010:Vac-VPT-09100
    #
    factory.add_turbo_pump(group, "RFQ-010:Vac-VPT-09100")

    #
    # Adding device RFQ-010:Vac-VPT-0A100
    #
    factory.add_turbo_pump(group, "RFQ-010:Vac-VPT-0A100")

    #
    # Adding device RFQ-010:Vac-VVG-06100
    #
    factory.add_vvg(group, "RFQ-010:Vac-VVG-06100", HW_2 = "RFQ-010:Vac-VPT-06100", Prs_1 = "RFQ-010:Vac-VGP-05100")

    #
    # Adding device RFQ-010:Vac-VVG-07100
    #
    factory.add_vvg(group, "RFQ-010:Vac-VVG-07100", HW_2 = "RFQ-010:Vac-VPT-07100", Prs_1 = "RFQ-010:Vac-VGP-05100")

    #
    # Adding device RFQ-010:Vac-VVG-08100
    #
    factory.add_vvg(group, "RFQ-010:Vac-VVG-08100", HW_2 = "RFQ-010:Vac-VPT-08100", Prs_1 = "RFQ-010:Vac-VGP-05100")

    #
    # Adding device RFQ-010:Vac-VVG-09100
    #
    factory.add_vvg(group, "RFQ-010:Vac-VVG-09100", HW_2 = "RFQ-010:Vac-VPT-09100", Prs_1 = "RFQ-010:Vac-VGP-05100")

    #
    # Adding device RFQ-010:Vac-VVG-0A100
    #
    factory.add_vvg(group, "RFQ-010:Vac-VVG-0A100", HW_2 = "RFQ-010:Vac-VPT-0A100", Prs_1 = "RFQ-010:Vac-VGP-05100")

    #
    # Adding device RFQ-010:Vac-VVA-00041
    #
    factory.add_vva(group, "RFQ-010:Vac-VVA-00041", HW_1 = "RFQ-010:Vac-VPDP-00041", Prs_2 = "RFQ-010:Vac-VGP-05100")


def create_mebt_devices(factory, plc):
    #
    # Adding device MEBT-010:Vac-VPSU-00010
    #
    factory.add_vpsu(plc, "MEBT-010:Vac-VPSU-00010")

    #
    # Adding device MEBT-010:Vac-PLCIO-01004
    #
    plcio = factory.add_plcio(plc, "MEBT-010:Vac-PLCIO-01004")

    #
    # Adding device MEBT-010:Vac-VPG-10001
    #
    group = factory.add_pumping_group(plc, "MEBT-010:Vac-VPG-10001")
    group.addLink("EPI[VACUUM_VAC-VPG-MEBT]", factory.git_repo(), epi_wc)

    #
    # Adding device MEBT-010:Vac-VEPP-00011 of type VACUUM_VAC-VEPP
    #
    vepp = group.addDevice("VACUUM_VAC-VEPP", "MEBT-010:Vac-VEPP-00011")

    #
    # Adding device MEBT-010:Vac-VPDP-00011
    #
    factory.add_primary_pump(vepp, "MEBT-010:Vac-VPDP-00011")

    #
    # Adding device MEBT-010:Vac-VPT-01100
    #
    factory.add_turbo_pump(group, "MEBT-010:Vac-VPT-01100")

    #
    # Adding device MEBT-010:Vac-VVG-01100
    #
    factory.add_vvg(group, "MEBT-010:Vac-VVG-01100")

    #
    # Adding device MEBT-010:Vac-VVA-00011
    #
    factory.add_vva(group, "MEBT-010:Vac-VVA-00011")

    #
    # Adding device MEBT-010:Vac-VPI-10000
    #
    factory.add_ion_pump_vpi(group, "MEBT-010:Vac-VPI-10000")

    #
    # Adding device MEBT-010:Vac-VPI-20000
    #
    factory.add_ion_pump_vpi(group, "MEBT-010:Vac-VPI-20000")

    #
    # Adding device MEBT-010:Vac-VPI-30000
    #
    factory.add_ion_pump_vpi(group, "MEBT-010:Vac-VPI-30000")

    #
    # Adding device MEBT-010:Vac-VPI-40000
    #
    factory.add_ion_pump_vpi(group, "MEBT-010:Vac-VPI-40000")

    #
    # Adding device MEBT-010:Vac-VPI-50000
    #
    factory.add_ion_pump_vpi(group, "MEBT-010:Vac-VPI-50000")


def create_dtl_devices(factory, plc):
    #
    # Adding device DTL-010:Vac-VPSU-00010
    #
    # For the gauges
    factory.add_vpsu(plc, "DTL-010:Vac-VPSU-00010")

    #
    # Adding device DTL-010:Vac-PLCIO-01005
    #
    plcio = factory.add_plcio(plc, "DTL-010:Vac-PLCIO-01005")

    #
    # Adding device DTL-010:Vac-VPSU-00020
    #
    # For the pumps
    factory.add_vpsu(plc, "DTL-010:Vac-VPSU-00020")

    #
    # Adding device DTL-010:Vac-PLCIO-01006
    #
    plcio = factory.add_plcio(plc, "DTL-010:Vac-PLCIO-01006")

    plcio = plc

    """
        VPDP
    """

    #
    # Adding device DTL-020:Vac-VPDP-00011
    #
    factory.add_primary_pump(plcio, "DTL-020:Vac-VPDP-00011")

    """
    #
    # Adding device DTL-040:Vac-VPDP-00011
    #
    factory.add_primary_pump(plcio, "DTL-040:Vac-VPDP-00011")
    """

    """
        VPT
    """

    #
    # Adding device DTL-010:Vac-VPT-01100
    #
    factory.add_turbo_pump(plcio, "DTL-010:Vac-VPT-01100")

    #
    # Adding device DTL-020:Vac-VPT-01100
    #
    factory.add_turbo_pump(plcio, "DTL-020:Vac-VPT-01100")

    """
    #
    # Adding device DTL-030:Vac-VPT-01100
    #
    factory.add_turbo_pump(plcio, "DTL-030:Vac-VPT-01100")

    #
    # Adding device DTL-040:Vac-VPT-01100
    #
    factory.add_turbo_pump(plcio, "DTL-040:Vac-VPT-01100")

    #
    # Adding device DTL-050:Vac-VPT-01100
    #
    factory.add_turbo_pump(plcio, "DTL-050:Vac-VPT-01100")
    """

    """
        VPN
    """

    #
    # Adding device DTL-010:Vac-VPN-10000
    #
    factory.add_ion_pump_vpn(plcio, "DTL-010:Vac-VPN-10000")

    #
    # Adding device DTL-010:Vac-VPN-20000
    #
    factory.add_ion_pump_vpn(plcio, "DTL-010:Vac-VPN-20000")

    #
    # Adding device DTL-010:Vac-VPN-30000
    #
    factory.add_ion_pump_vpn(plcio, "DTL-010:Vac-VPN-30000")

    #
    # Adding device DTL-010:Vac-VPN-40000
    #
    factory.add_ion_pump_vpn(plcio, "DTL-010:Vac-VPN-40000")

    #
    # Adding device DTL-020:Vac-VPN-10000
    #
    factory.add_ion_pump_vpn(plcio, "DTL-020:Vac-VPN-10000")

    #
    # Adding device DTL-020:Vac-VPN-20000
    #
    factory.add_ion_pump_vpn(plcio, "DTL-020:Vac-VPN-20000")

    #
    # Adding device DTL-020:Vac-VPN-30000
    #
    factory.add_ion_pump_vpn(plcio, "DTL-020:Vac-VPN-30000")

    #
    # Adding device DTL-020:Vac-VPN-40000
    #
    factory.add_ion_pump_vpn(plcio, "DTL-020:Vac-VPN-40000")

    """
    #
    # Adding device DTL-030:Vac-VPN-10000
    #
    factory.add_ion_pump_vpn(plcio, "DTL-030:Vac-VPN-10000")

    #
    # Adding device DTL-030:Vac-VPN-20000
    #
    factory.add_ion_pump_vpn(plcio, "DTL-030:Vac-VPN-20000")

    #
    # Adding device DTL-030:Vac-VPN-30000
    #
    factory.add_ion_pump_vpn(plcio, "DTL-030:Vac-VPN-30000")

    #
    # Adding device DTL-030:Vac-VPN-40000
    #
    factory.add_ion_pump_vpn(plcio, "DTL-030:Vac-VPN-40000")

    #
    # Adding device DTL-040:Vac-VPN-10000
    #
    factory.add_ion_pump_vpn(plcio, "DTL-040:Vac-VPN-10000")

    #
    # Adding device DTL-040:Vac-VPN-20000
    #
    factory.add_ion_pump_vpn(plcio, "DTL-040:Vac-VPN-20000")

    #
    # Adding device DTL-040:Vac-VPN-30000
    #
    factory.add_ion_pump_vpn(plcio, "DTL-040:Vac-VPN-30000")

    #
    # Adding device DTL-040:Vac-VPN-40000
    #
    factory.add_ion_pump_vpn(plcio, "DTL-040:Vac-VPN-40000")

    #
    # Adding device DTL-050:Vac-VPN-10000
    #
    factory.add_ion_pump_vpn(plcio, "DTL-050:Vac-VPN-10000")

    #
    # Adding device DTL-050:Vac-VPN-20000
    #
    factory.add_ion_pump_vpn(plcio, "DTL-050:Vac-VPN-20000")

    #
    # Adding device DTL-050:Vac-VPN-30000
    #
    factory.add_ion_pump_vpn(plcio, "DTL-050:Vac-VPN-30000")

    #
    # Adding device DTL-050:Vac-VPN-40000
    #
    factory.add_ion_pump_vpn(plcio, "DTL-050:Vac-VPN-40000")
    """

    """
        VVG
    """

    #
    # Adding device DTL-010:Vac-VVG-01100
    #
    factory.add_vvg(plcio, "DTL-010:Vac-VVG-01100")

    #
    # Adding device DTL-020:Vac-VVG-01100
    #
    factory.add_vvg(plcio, "DTL-020:Vac-VVG-01100")

    """
    #
    # Adding device DTL-030:Vac-VVG-01100
    #
    factory.add_vvg(plcio, "DTL-030:Vac-VVG-01100")

    #
    # Adding device DTL-040:Vac-VVG-01100
    #
    factory.add_vvg(plcio, "DTL-040:Vac-VVG-01100")

    #
    # Adding device DTL-050:Vac-VVG-01100
    #
    factory.add_vvg(plcio, "DTL-050:Vac-VVG-01100")
    """

    """
        VVA
    """

    #
    # Adding device DTL-020:Vac-VVA-00011
    #
    factory.add_vva(plcio, "DTL-020:Vac-VVA-00011")

    """
    #
    # Adding device DTL-040:Vac-VVA-00011
    #
    factory.add_vva(plcio, "DTL-040:Vac-VVA-00011")
    """


create_ioc = create_process_plc_ioc



if __name__ == "__main__":
    #
    # Create factory
    #
    factory = initialize_factory()

    #
    # Create IOC
    #
    ioc = create_process_plc_ioc(factory)

    #
    # Saving the created CCDB
    #
    factory.save(ioc.name())
