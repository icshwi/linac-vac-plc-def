#!/usr/bin/env python

import os
import sys

thisdir = os.path.abspath(os.path.dirname(__file__))
sys.path.append(thisdir)

import vac_factory_helpers


epi_wc = os.path.dirname(thisdir)
epi_wc = None
HOST = "vacs-accv-vm-ioc"
EPICS_VERSION = vac_factory_helpers.VacuumFactory.EPICS_VERSION
E3_REQUIRE_VERSION = vac_factory_helpers.VacuumFactory.E3_REQUIRE_VERSION


def initialize_factory(factory = None):
    if factory is None:
        factory = vac_factory_helpers.VacuumFactory(epi_wc)

    factory.add_gauge_devtypes()
    factory.add_turbopump_devtypes()

    factory.rfq_moxa_1 = "moxa-vac-rfq-1.tn.esss.lu.se"
    factory.rfq_moxa_2 = "moxa-vac-rfq-2.tn.esss.lu.se"

    return factory


GAUGE_IOC = "VacS-RFQ:SC-IOC-110"
def create_rfq_gauge_ioc(factory, name = GAUGE_IOC, host = HOST):
    rfq_moxa_1 = factory.rfq_moxa_1
    rfq_moxa_2 = factory.rfq_moxa_2

    #
    # Adding IOC
    #
    ioc = factory.add_ioc(name, host, description="IOC for RFQ vacuum gauge controllers and gauges")

    #
    # Adding device RFQ-010:Vac-VEG-10001 of type VACUUM_VAC-VEG|VAC-VEVMC
    #
    mks = factory.add_mks(ioc, "RFQ-010:Vac-VEG-10001", rfq_moxa_1, 4001, BOARD_A_SERIAL_NUMBER = "1807161527", BOARD_B_SERIAL_NUMBER = "1812041021", BOARD_C_SERIAL_NUMBER = "1812041005")
    factory.add_mks_vgp(mks, "RFQ-010:Vac-VGP-10000", "A1")
    factory.add_mks_vgc(mks, "RFQ-010:Vac-VGC-10000", "B1")
    factory.add_mks_vgc(mks, "RFQ-010:Vac-VGC-40000", "C1")

    #
    # Adding device RFQ-010:Vac-VEG-10002 of type VACUUM_VAC-VEG|VAC-VEVMC
    #
    mks = factory.add_mks(ioc, "RFQ-010:Vac-VEG-10002", rfq_moxa_1, 4002, BOARD_A_SERIAL_NUMBER = "1807121009", BOARD_B_SERIAL_NUMBER = "1609080821", BOARD_C_SERIAL_NUMBER = "1505070814")
    factory.add_mks_vgc(mks, "RFQ-010:Vac-VGC-20000", "B1")
    factory.add_mks_vgc(mks, "RFQ-010:Vac-VGC-30000", "C1")

    #
    # Adding device RFQ-010:Vac-VEG-01100 of type VACUUM_VAC-VEG|VAC-VEVMC
    #
    mks = factory.add_mks(ioc, "RFQ-010:Vac-VEG-01100", rfq_moxa_1, 4003, BOARD_A_SERIAL_NUMBER = "1807121113", BOARD_B_SERIAL_NUMBER = "1505070713", BOARD_C_SERIAL_NUMBER = "1609091039")
    factory.add_mks_vgp(mks, "RFQ-010:Vac-VGP-05100", "A1")
    factory.add_mks_vgp(mks, "RFQ-010:Vac-VGP-06100", "A2")

    return ioc


RGA_IOC = "VacS-RFQ:SC-IOC-119"
def create_rfq_rga_ioc(factory, name = RGA_IOC, host = HOST):
    rfq_moxa_1 = factory.rfq_moxa_1
    rfq_moxa_2 = factory.rfq_moxa_2

    #
    # Adding IOC
    #
    ioc = factory.add_ioc(name, host, description="IOC for RFQ vacuum residual gas analyzer")

    #
    # Adding device RFQ-010:Vac-VERA-40001 of type VACUUM_VAC-VERA
    #
    vera = factory.add_vera(ioc, "RFQ-010:Vac-VERA-40001", rfq_moxa_1, 4004)
    factory.add_rga(vera, "RFQ-010:Vac-VGR-40000", rfq_moxa_1, 4004)

    return ioc


TCP_IOC = "VacS-RFQ:SC-IOC-130"
def create_rfq_turbopump_ioc(factory, name = TCP_IOC, host = HOST):
    rfq_moxa_1 = factory.rfq_moxa_1
    rfq_moxa_2 = factory.rfq_moxa_2

    #
    # Adding IOC
    #
    ioc = factory.add_ioc(name, host, description="IOC for RFQ vacuum turbopumps")

    #
    # Adding device RFQ-010:Vac-VEPT-01100 of type VACUUM_VAC-VEPT
    #
    tcp350 = factory.add_tcp350(ioc, "RFQ-010:Vac-VEPT-01100", rfq_moxa_2, 4001)
    factory.add_tcp_pump(tcp350, "RFQ-010:Vac-VPT-01100")

    #
    # Adding device RFQ-010:Vac-VEPT-02100 of type VACUUM_VAC-VEPT
    #
    tcp350 = factory.add_tcp350(ioc, "RFQ-010:Vac-VEPT-02100", rfq_moxa_2, 4002)
    factory.add_tcp_pump(tcp350, "RFQ-010:Vac-VPT-02100")

    #
    # Adding device RFQ-010:Vac-VEPT-03100 of type VACUUM_VAC-VEPT
    #
    tcp350 = factory.add_tcp350(ioc, "RFQ-010:Vac-VEPT-03100", rfq_moxa_2, 4003)
    factory.add_tcp_pump(tcp350, "RFQ-010:Vac-VPT-03100")

    #
    # Adding device RFQ-010:Vac-VEPT-04100 of type VACUUM_VAC-VEPT
    #
    tcp350 = factory.add_tcp350(ioc, "RFQ-010:Vac-VEPT-04100", rfq_moxa_2, 4004)
    factory.add_tcp_pump(tcp350, "RFQ-010:Vac-VPT-04100")

    #
    # Adding device RFQ-010:Vac-VEPT-05100 of type VACUUM_VAC-VEPT
    #
    tcp350 = factory.add_tcp350(ioc, "RFQ-010:Vac-VEPT-05100", rfq_moxa_2, 4005)
    factory.add_tcp_pump(tcp350, "RFQ-010:Vac-VPT-05100")

    #
    # Adding device RFQ-010:Vac-VEPT-06100 of type VACUUM_VAC-VEPT
    #
    tcp350 = factory.add_tcp350(ioc, "RFQ-010:Vac-VEPT-06100", rfq_moxa_2, 4006)
    factory.add_tcp_pump(tcp350, "RFQ-010:Vac-VPT-06100")

    #
    # Adding device RFQ-010:Vac-VEPT-07100 of type VACUUM_VAC-VEPT
    #
    tcp350 = factory.add_tcp350(ioc, "RFQ-010:Vac-VEPT-07100", rfq_moxa_2, 4007)
    factory.add_tcp_pump(tcp350, "RFQ-010:Vac-VPT-07100")

    #
    # Adding device RFQ-010:Vac-VEPT-08100 of type VACUUM_VAC-VEPT
    #
    tcp350 = factory.add_tcp350(ioc, "RFQ-010:Vac-VEPT-08100", rfq_moxa_2, 4008)
    factory.add_tcp_pump(tcp350, "RFQ-010:Vac-VPT-08100")

    #
    # Adding device RFQ-010:Vac-VEPT-09100 of type VACUUM_VAC-VEPT
    #
    tcp350 = factory.add_tcp350(ioc, "RFQ-010:Vac-VEPT-09100", rfq_moxa_2, 4009)
    factory.add_tcp_pump(tcp350, "RFQ-010:Vac-VPT-09100")

    #
    # Adding device RFQ-010:Vac-VEPT-0A100 of type VACUUM_VAC-VEPT
    #
    tcp350 = factory.add_tcp350(ioc, "RFQ-010:Vac-VEPT-0A100", rfq_moxa_2, 4010)
    factory.add_tcp_pump(tcp350, "RFQ-010:Vac-VPT-0A100")

    return ioc


RFQ_IOC = "VacS-RFQ:SC-IOC-100"
def create_rfq_ioc(factory, name = RFQ_IOC, host = HOST):
    #
    # Adding IOC
    #
    ioc = factory.add_ioc(name, host)

    create_rfq_gauge_ioc(factory, ioc, host)
    create_rfq_rga_ioc(factory, ioc, host)
    create_rfq_turbopump_ioc(factory, ioc, host)

    return ioc


create_ioc = create_rfq_ioc



if __name__ == "__main__":
    #
    # Create factory
    #
    factory = initialize_factory()

    #
    # Create Gauge IOC
    #
    create_rfq_gauge_ioc(factory)

    #
    # Create RGA IOC
    #
    create_rfq_rga_ioc(factory)

    #
    # Create Turbopump IOC
    #
    create_rfq_turbopump_ioc(factory)

    #
    # Saving the created CCDB
    #
    factory.save("VacS-RFQ:SC-IOC-all")
