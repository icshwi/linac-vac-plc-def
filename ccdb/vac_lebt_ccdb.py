#!/usr/bin/env python

import os
import sys

thisdir = os.path.abspath(os.path.dirname(__file__))
sys.path.append(thisdir)

import vac_factory_helpers


epi_wc = os.path.dirname(thisdir)
epi_wc = None
HOST = "vacs-accv-vm-ioc"
EPICS_VERSION = vac_factory_helpers.VacuumFactory.EPICS_VERSION
E3_REQUIRE_VERSION = vac_factory_helpers.VacuumFactory.E3_REQUIRE_VERSION


def initialize_factory(factory = None):
    if factory is None:
        factory = vac_factory_helpers.VacuumFactory(epi_wc)

    factory.add_gauge_devtypes()
    factory.add_turbopump_devtypes()

    factory.isrc_moxa = "isrc-moxa-mks.tn.esss.lu.se"
    factory.lebt_moxa = "lebt-vac-sec-10001.tn.esss.lu.se"

    return factory


SWITCHYARD_IOC = "VacS-LEBT:SC-IOC-108"
def create_lebt_switchyard_ioc(factory, name = SWITCHYARD_IOC, host = HOST):
    #
    # Adding IOC of type IOC
    #
    ioc = factory.add_ioc(name, host, description="IOC for LEBT vacuum pumping groups")

    #
    # Adding device LEBT-010:Vac-VPG-10001 of type VACUUM_VAC-VPG
    #
    yard = factory.add_switchyard(ioc, "LEBT-010:Vac-VPG-10001", "VacS-ACCV:Vac-PLC-01001", "LEBT-010:Vac-VGP-10000", "LEBT-010:Vac-VGP-00021")

    #
    # Adding device LEBT-010:Vac-VPG-20001 of type VACUUM_VAC-VPG
    #
    yard = factory.add_switchyard(ioc, "LEBT-010:Vac-VPG-20001", "VacS-ACCV:Vac-PLC-01001", "LEBT-010:Vac-VGP-30000", "LEBT-010:Vac-VGP-00081")

    return ioc


ISRC_GAUGE_IOC = "VacS-ISrc:SC-IOC-110"
def create_isrc_gauge_ioc(factory, name = ISRC_GAUGE_IOC, host = HOST):
    isrc_moxa = factory.isrc_moxa

    #
    # Adding IOC
    #
    ioc = factory.add_ioc(name, host, description="IOC for ISrc vacuum gauge controllers and gauges")

    #
    # Adding device ISrc-010:Vac-VEVMC-01100 of type VACUUM_VAC-VEG|VAC-VEVMC
    #
    mks = factory.add_mks(ioc, "ISrc-010:Vac-VEVMC-01100", isrc_moxa, 4001, BOARD_A_SERIAL_NUMBER = "1601141250", BOARD_B_SERIAL_NUMBER = "1511050734")
    factory.add_mks_vvmc(mks, "ISrc-010:Vac-VVMC-01100", "A1")
    factory.add_mks_vgd(mks, "ISrc-010:Vac-VGD-01100", "B1")

    return ioc


LEBT_GAUGE_IOC = "VacS-LEBT:SC-IOC-110"
def create_lebt_gauge_ioc(factory, name = LEBT_GAUGE_IOC, host = HOST):
    lebt_moxa = factory.lebt_moxa

    #
    # Adding IOC
    #
    ioc = factory.add_ioc(name, host, description="IOC for LEBT vacuum gauge controllers and gauges")

    #
    # Adding device LEBT-010:Vac-VEG-00011 of type VACUUM_VAC-VEG|VAC-VEVMC
    #
    mks = factory.add_mks(ioc, "LEBT-010:Vac-VEG-00011", lebt_moxa, 4001, BOARD_A_SERIAL_NUMBER = "1509231040", BOARD_B_SERIAL_NUMBER = "1609141318")
    factory.add_mks_vgp(mks, "LEBT-010:Vac-VGP-00021", "A1")
    factory.add_mks_vgp(mks, "LEBT-010:Vac-VGP-00081", "A2")

    #
    # Adding device LEBT-010:Vac-VEG-10001 of type VACUUM_VAC-VEG|VAC-VEVMC
    #
    mks = factory.add_mks(ioc, "LEBT-010:Vac-VEG-10001", lebt_moxa, 4002, BOARD_A_SERIAL_NUMBER = "1509231115", BOARD_B_SERIAL_NUMBER = "1609141305", BOARD_C_SERIAL_NUMBER = "1412221837")
    factory.add_mks_vgp(mks, "LEBT-010:Vac-VGP-10000", "A1")
    factory.add_mks_vgp(mks, "LEBT-010:Vac-VGP-30000", "A2")
    factory.add_mks_vgc(mks, "LEBT-010:Vac-VGC-10000", "B1")
    factory.add_mks_vgc(mks, "LEBT-010:Vac-VGC-30000", "C1")

#   #
#   # Adding device LEBT-010:Vac-VEG-99999 of type VACUUM_VAC-VEG|VAC-VEVMC
#   #
#   mks = factory.add_mks(ioc, "LEBT-010:Vac-VEG-99999", lebt_moxa, 4004)
#   factory.add_mks_vgc(mks, "LEBT-010:Vac-VGC-99999", "B1", 2, 1)
#   factory.add_mks_vgp(mks, "LEBT-010:Vac-VGP-99999", "A1")

    #
    # Adding device LEBT-010:Vac-VEVMC-10001 of type VACUUM_VAC-VEG|VAC-VEVMC
    #
    mks = factory.add_mks(ioc, "LEBT-010:Vac-VEVMC-10001", lebt_moxa, 4003, BOARD_A_SERIAL_NUMBER = "1601141134", BOARD_B_SERIAL_NUMBER = "1511060832")
    factory.add_mks_vvmc(mks, "LEBT-010:Vac-VVMC-01100", "A1")
    factory.add_mks_vgd(mks, "LEBT-010:Vac-VGD-10000", "B1")

    return ioc


RGA_IOC = "VacS-LEBT:SC-IOC-119"
def create_lebt_rga_ioc(factory, name = RGA_IOC, host = HOST):
    lebt_moxa = factory.lebt_moxa

    #
    # Adding IOC
    #
    ioc = factory.add_ioc(name, host, description="IOC for LEBT vacuum residual gas analyzer")

    #
    # Adding device LEBT-010:Vac-VERA-10001 of type VACUUM_VAC-VERA
    #
    vera = factory.add_vera(ioc, "LEBT-010:Vac-VERA-10001", lebt_moxa, 4013)
    factory.add_rga(vera, "LEBT-010:Vac-VGR-10000", lebt_moxa, 4013)

    return ioc


TURBOPUMP_IOC = "VacS-LEBT:SC-IOC-130"
def create_lebt_turbopump_ioc(factory, name = TURBOPUMP_IOC, host = HOST):
    lebt_moxa = factory.lebt_moxa

    #
    # Adding IOC
    #
    ioc = factory.add_ioc(name, host, description="IOC for LEBT vacuum turbopumps")

    #
    # Adding device LEBT-010:Vac-VEPT-02100 of type VACUUM_VAC-VEPT
    #
    leybold = factory.add_leybold(ioc, "LEBT-010:Vac-VEPT-02100", lebt_moxa, 4005)
    factory.add_leybold_pump(leybold, "LEBT-010:Vac-VPT-02100")

    #
    # Adding device LEBT-010:Vac-VEPT-03100 of type VACUUM_VAC-VEPT
    #
    leybold = factory.add_leybold(ioc, "LEBT-010:Vac-VEPT-03100", lebt_moxa, 4006)
    factory.add_leybold_pump(leybold, "LEBT-010:Vac-VPT-03100")

    #
    # Adding device LEBT-010:Vac-VEPT-06100 of type VACUUM_VAC-VEPT
    #
    leybold = factory.add_leybold(ioc, "LEBT-010:Vac-VEPT-06100", lebt_moxa, 4007)
    factory.add_leybold_pump(leybold, "LEBT-010:Vac-VPT-06100")

    #
    # Adding device LEBT-010:Vac-VEPT-07100 of type VACUUM_VAC-VEPT
    #
    leybold = factory.add_leybold(ioc, "LEBT-010:Vac-VEPT-07100", lebt_moxa, 4008)
    factory.add_leybold_pump(leybold, "LEBT-010:Vac-VPT-07100")


#   #
#   # Adding device LEBT-010:Vac-VEPT-99998 of type VACUUM_VAC-VEPT
#   #
#   leybold = factory.add_leybold(ioc, "LEBT-010:Vac-VEPT-99998", lebt_moxa, 4009)
#   factory.add_leybold_pump(leybold, "LEBT-010:Vac-VPT-99998")
#
#   #
#   # Adding device LEBT-010:Vac-VEPT-99999 of type VACUUM_VAC-VEPT
#   #
#   leybold = factory.add_leybold(ioc, "LEBT-010:Vac-VEPT-99999", lebt_moxa, 4010)
#   factory.add_leybold_pump(leybold, "LEBT-010:Vac-VPT-99999")

    return ioc


LEBT_IOC = "VacS-LEBT:SC-IOC-100"
def create_lebt_ioc(factory, name = LEBT_IOC, host = HOST):
    #
    # Adding IOC
    #
    ioc = factory.add_ioc(name, host)

    create_lebt_switchyard_ioc(factory, ioc, host)
    create_lebt_gauge_ioc(factory, ioc, host)
    create_lebt_rga_ioc(factory, ioc, host)
    create_lebt_turbopump_ioc(factory, ioc, host)

    return ioc


create_ioc = create_lebt_ioc



if __name__ == "__main__":
    #
    # Create factory
    #
    factory = initialize_factory()

    #
    # Create Switchyard IOC
    #
    create_lebt_switchyard_ioc(factory)

    #
    # Create Gauge IOC
    #
    create_isrc_gauge_ioc(factory)
    create_lebt_gauge_ioc(factory)

    #
    # Create RGA IOC
    #
    create_lebt_rga_ioc(factory)

    #
    # Create Turbopump IOC
    #
    create_lebt_turbopump_ioc(factory)

    #
    # Saving the created CCDB
    #
    factory.save("VacS-LEBT:SC-IOC-all")
